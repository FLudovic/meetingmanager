module.exports = {
    EmailValidator: function(v) {
        // Controls if condition is respected
        switch(true) {
            // Ensure email is not empty
            case /^\s*$/.test(v):
                return false;
            // Ensure email respect xxx@xxx.xxx pattern
            case !/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(v):
                return false;
        }
    },
    NameValidator: function(v) {
         // Controls if condition is respected
         switch(true) {
            // Ensure firstName or lastName are not empty
            case /^\s*$/.test(v):
                return false;
            // Ensure firstName or lastName are respecting a-z/A-Z/,. ' pattern
            case !/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u.test(v):
                return false;
        }
    },
    CompanyNameValidator: function(v) {
        // Controls if condition is respected
        switch(true) {
            // Return message if company name is empty
            case /^\s*$/.test(v):
                return false;
            // Ensure company name is respecting pattern
            case !/^[.@&]?[a-zA-Z0-9 ]+[ !.@&()]?[ a-zA-Z0-9!()]+/.test(v):
                return false;
        }
    },
    PasswordValidator: function(v) {
         // Controls if condition is respected
         switch(true) {
            // Ensure firstName or lastName are not empty
            case /^\s*$/.test(v):
                return false;
        }
    },
    PhoneValidator: function(v) {
        // Controls if condition is respected
        switch(true) {
            // Controls if phone is empty
            case /^\s*$/.test(v):
                return false;
             // Ensure phone number respect 0000000000 or 33000000000 pattern
            case !/^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/.test(v):
                return false;
        }
    }
};
