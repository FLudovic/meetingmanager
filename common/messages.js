module.exports = {
    EmailErrorMessages: function(v) {
        // Controls if condition is respected
        switch(true) {
            // Return message if email is empty
            case /^\s*$/.test(v.value):
                return `cannot be empty.`;
            // Ensure email respect xxx@xxx.xxx pattern
            case !/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(v.value):
                return `${v.value} is not a valid email pattern.`;
        }
    },
    NameErrorMessages: function(v) {
         // Controls if condition is respected
         switch(true) {
            // Return message if firstName or lastName empty
            case /^\s*$/.test(v.value):
                return `cannot be empty.`;
            // Ensure firstName or lastName are respecting a-z/A-Z/,. ' pattern
            case !/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u.test(v.value):
                return `${v.value} is not a valid pattern.`;
        }
    },
    CompanyNameErrorMessages: function(v) {
        // Controls if condition is respected
        switch(true) {
            // Return message if company name is empty
            case /^\s*$/.test(v.value):
                return `cannot be empty.`;
            // Ensure company name is respecting pattern
            case !/^[.@&]?[a-zA-Z0-9 ]+[ !.@&()]?[ a-zA-Z0-9!()]+/.test(v.value):
                return `${v.value} is not a valid company name pattern.`;
        }
    },
    PasswordErrorMessages: function(v) {
        // Controls if condition is respected
        switch(true) {
            // Return message if firstName or lastName empty
            case /^\s*$/.test(v.value):
                return `cannot be empty.`;
        }
    },
    PhoneErrorMessages: function(v) {
        // Controls if condition is respected
        switch(true) {
            // Return message if phone is empty
            case /^\s*$/.test(v.value):
                return `cannot be empty.`;
            // Ensure phone number respect 0000000000 or 33000000000 pattern
            case !/^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/.test(v.value):
                return `${v.value} is not a valid pattern.`;
        }
    }
};
