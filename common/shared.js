// Write file
const fs = require("fs");

// Local color file
const c = require("../configs/colors");
const { v4: uuidv4 } = require("uuid");

// Manage environment variables
const conf = require("../configs/conf");

const fetch = require("node-fetch");
const { Dropbox } = require("dropbox");
const accessToken = conf.dropboxAccessToken;
const sm = require("../configs/status-messages");

const OrganizationSchema = require("../models/organization-model");

const dbx = new Dropbox({
  accessToken,
  fetch,
});

module.exports = {
  logger: function (operation, status, user) {
    user = (user || "Server") + " ";
    switch (status) {
      case "neutral":
        console.log(c.fgWhite + "[+] " + user + operation + c.reset);
        break;
      case "info":
        console.log(c.info + "[i] " + user + operation + c.reset);
        break;
      case "warning":
        console.log(c.fgYellow + "[-] " + user + operation + c.reset);
        break;
      case "error":
        console.log(c.fgRed + "[!] " + user + operation + c.reset);
        break;
      default:
        console.log(
          c.fgWhite + "[unknown logger status] " + user + operation + c.reset
        );
    }
    WriteLogs(user, operation, status);
  },
  storeLogoToDropbox: function (
    token_id,
    newOrganization,
    logo,
    startPath,
    callback
  ) {
    let pictureBase64 = logo;
    let buffer = Buffer.from(pictureBase64, "base64");
    let path = startPath + uuidv4() + ".png";
    // Upload file on dropbox
    dbx
      .filesUpload({
        path: path,
        contents: buffer,
      })
      .then(function (response) {
        // Upload success, so we generate link for insert in db
        dbx
          .sharingCreateSharedLinkWithSettings({
            path: path,
          })
          .then((response) => {
            if (response.result.url != null) {
              newOrganization.logoPath = response.result.url;
              newOrganization
                .save()
                .then((res) => {
                  // On success
                  module.exports.logger(
                    sm.saveOrganization,
                    "neutral",
                    token_id
                  );
                  callback(200, {
                    _id: res._id,
                    name: res.name,
                    address: res.address,
                    employeeNumber: res.employeeNumber,
                    date: res.date,
                    logoPath: response.result.url,
                  });
                })
                .catch((err) => {
                  // On error
                  module.exports.logger(err.message, "error");
                  callback(400, { message: err.message });
                });
            } else {
              // If logo save error
              module.exports.logger(
                sm.saveOrganization + sm.invalidImage,
                "warning",
                token_id
              );
              callback(400, {
                message: sm.saveOrganizationError + sm.invalidImage,
              });
            }
          })
          .catch((err) => {
            // On error
            module.exports.logger(err.message, "error");
            callback(500, { message: sm.savePictureError });
          });
      })
      .catch(function (error) {
        // Error Upload
        module.exports.logger(
          sm.savePicture + sm.invalidImage,
          "warning" + error,
          token_id
        );
        callback(400, { message: sm.savePictureError + sm.invalidImage });
      });
  },
  updateLogoToDropbox: function (token_id, body, id, startPath, callback) {
    const newOrganization = {
      name: body.name,
      address: body.address,
      employeeNumber: body.employeeNumber,
    };

    const options = {
      runValidators: true,
      context: "query",
      returnOriginal: false,
    };

    if (body.logo) {
      // map user fields
      let pictureBase64 = body.logo;
      let buffer = Buffer.from(pictureBase64, "base64");
      let path = startPath + uuidv4() + ".png";
      dbx
        .filesUpload({
          path: path,
          contents: buffer,
        })
        .then(function (response) {
          // Upload success, so we generate link for insert in db
          dbx
            .sharingCreateSharedLinkWithSettings({
              path: path,
            })
            .then((response) => {
              if (response.result.url != null) {
                // const pathToDelete =
                //   "/logos/" +
                //   response.result.url.split("/").pop().split("?")[0];

                OrganizationSchema.findOne({
                  _id: id,
                }).then(function (res) {
                  if (res) {

                    const pathToDelete =
                      "/logos/" +
                      res.logoPath.split("/").pop().split("?")[0];

                    dbx
                      .filesDeleteV2({
                        path: pathToDelete,
                      })
                      .then(function (res) {
                        console.log("Deleted the previous logo");
                        newOrganization.logoPath = response.result.url;

                        // On success
                        module.exports.logger(
                          sm.saveOrganization,
                          "neutral",
                          token_id
                        );
                        

                        // Update organization with the new model and activate validation options from schema
                        OrganizationSchema.findOneAndUpdate(
                          { _id: id },
                          newOrganization,
                          options,
                          function (err, res) {
                            if (!err) {
                              if (res === null) {
                                module.exports.logger(
                                  sm.updateOrganization + sm.invalidUpdate,
                                  "warning",
                                  token_id
                                );
                                callback(404, {
                                  message:
                                    sm.updateOrganizationError +
                                    sm.invalidUpdate,
                                });
                              } else {
                                // On success
                                module.exports.logger(
                                  sm.updateOrganization,
                                  "neutral",
                                  token_id
                                );
                                console.log("Updated the new logo and organization property");
                                callback(200, {
                                  _id: res._id,
                                  name: res.name,
                                  address: res.address,
                                  employeeNumber: res.employeeNumber,
                                  date: res.date,
                                  logoPath: res.logoPath,
                                });
                              }
                            } else {
                              // On validation error
                              module.exports.logger(
                                sm.updateOrganization + err.message,
                                "warning",
                                token_id
                              );
                              callback(400, { message: err.message });
                            }
                          }
                        );
                      })
                      .catch(function (err) {
                        console.log("err", err);
                      });
                  } else {
                    callback(404, "ça a pas marché ");
                  }
                });
              } else {
                // If logo save error
                module.exports.logger(
                  sm.saveOrganization + sm.invalidImage,
                  "warning",
                  token_id
                );
                callback(400, {
                  message: sm.saveOrganizationError + sm.invalidImage,
                });
              }
            })
            .catch((err) => {
              // On error
              module.exports.logger(err.message, "error");
              callback(500, { message: sm.savePictureError });
            });
        })
        .catch(function (error) {
          // Error Upload
          module.exports.logger(
            sm.savePicture + sm.invalidImage,
            "warning" + error,
            token_id
          );
          callback(400, { message: sm.savePictureError + sm.invalidImage });
        });
    } else {
      OrganizationSchema.findOneAndUpdate(
        { _id: id },
        newOrganization,
        options,
        function (err, res) {
          if (!err) {
            if (res === null) {
              module.exports.logger(
                sm.updateOrganization + sm.invalidUpdate,
                "warning",
                token_id
              );
              callback(404, {
                message:
                  sm.updateOrganizationError +
                  sm.invalidUpdate,
              });
            } else {
              // On success
              module.exports.logger(
                sm.updateOrganization,
                "neutral",
                token_id
              );
              console.log("Updated the new logo and organization property");
              callback(200, {
                _id: res._id,
                name: res.name,
                address: res.address,
                employeeNumber: res.employeeNumber,
                date: res.date,
                logoPath: res.logoPath,
              });
            }
          } else {
            // On validation error
            module.exports.logger(
              sm.updateOrganization + err.message,
              "warning",
              token_id
            );
            callback(400, { message: err.message });
          }
        }
      );
    }
  },
  storeImageToDropbox: function (
    token_id,
    newClientPicture,
    body,
    startPath,
    callback
  ) {
    let pictureBase64 = body.picture;
    let buffer = Buffer.from(pictureBase64, "base64");
    let path = startPath + uuidv4() + ".png";
    // Upload file on dropbox
    dbx
      .filesUpload({
        path: path,
        contents: buffer,
      })
      .then(function (response) {
        // Upload success, so we generate link for insert in db
        dbx
          .sharingCreateSharedLinkWithSettings({
            path: path,
          })
          .then((response) => {
            newClientPicture.picturePath = response.result.url;
            newClientPicture
              .save()
              .then((resp) => {
                // On success
                module.exports.logger(sm.savePicture, "neutral", token_id);
                callback(200, newClientPicture);
              })
              .catch((err) => {
                // On error
                module.exports.logger(err.message, "error");
                callback(500, { message: sm.savePictureError });
              });
          })
          .catch((err) => {
            console.log("err", err);
          });
      })
      .catch(function (error) {
        // Error Upload
        module.exports.logger(
          sm.savePicture + sm.invalidImage,
          "warning" + error,
          token_id
        );
        callback(400, { message: sm.savePictureError + sm.invalidImage });
      });
  },
  compareDates: function (start, end) {
    const d = {
      years: 0,
      months: 0,
      days: 0,
      hours: 0,
      minutes: 1,
      seconds: 0,
    };

    // append start date with delay
    start.setFullYear(start.getFullYear() + d.years);
    start.setMonth(start.getMonth() + d.months);
    start.setDate(start.getDate() + d.days);
    start.setHours(start.getHours() + d.hours);
    start.setMinutes(start.getMinutes() + d.minutes);
    start.setSeconds(start.getSeconds() + d.seconds);

    // return the difference between start date and end date
    return start < end;
  },
  defaultNbPagination: 5,
  defaultNbPage: 0,
};

function WriteLogs(user, operation, status) {
  formattedLog = {
    date: new Date(Date.now()).toUTCString(),
    status: "[" + status.toUpperCase() + "]",
    operation: user + " " + operation,
  };

  formattedLog = JSON.stringify(formattedLog);

  // Uncomment to see log file in console
  //console.log(formattedLog);

  // TODO change file path every hour / day
  fs.appendFile("./public/logs/log.txt", formattedLog + "\r\n", (err) => {
    if (err) {
      console.error(err);
      return;
    }
    //file written successfully
  });
}
