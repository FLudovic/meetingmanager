const jwt = require("jsonwebtoken");

// Conf
const conf = require("../configs/conf");

const env = require("dotenv").config({ path: "./.env" });

const verifyToken = (req, res, next) => {
  const token = 
    req.body.token || req.query.token || req.headers["x-access-token"];

  if (!token) {
    return res.status(401).send({
      message : "A token is required for authentication"});
  }
  try {
    const decoded  = jwt.verify(token, conf.TOKEN_KEY);
          req.user = decoded;
  } catch (err) {
    if (err.name == "TokenExpiredError") {
      return res.status(401).send({
        message : "Your token has expired"
      });
    }
    return res.status(401).send({
      message : "Invalid Token"});
  }
  return next();
};

const generateToken = (adminObject) => {
  let   email = adminObject.email;
  const token = jwt.sign(
    { admin_id: adminObject._id, email },
    conf.TOKEN_KEY,
    {
      expiresIn: "20m",
    }
  );
  return token;
}

module.exports = {verifyToken, generateToken};
