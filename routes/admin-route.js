// Express routing
const express = require("express");
const router  = express.Router();

// Import jsonwebtoken to manage token
const { verifyToken } = require("../common/auth");

// Admin controller manage routes functions and database requests
const { AdminController } = require("../controllers/admin-controller");

const { defaultNbPage } = require('../common/shared');

///////////////////////////////////////////////////////////////////
//  GET ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {get}/admin Get all
 * @apiName GetAdmins
 * @apiGroup Admin
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {date} lastUpdate Last time admin created an admin account
 * @apiSuccess {ObjectId} _id Admin unique identifier
 * @apiSuccess {string} firstName Admin firstName
 * @apiSuccess {string} lastName Admin lastName
 * @apiSuccess {string} password Admin password hash
 * @apiSuccess {email} email Admin email
 * @apiSuccess {date} creationDate Admin creation date
 * @apiSuccess {string} token Admin token
 * @apiSuccess {int} __v Version
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *        {
 *          "lastUpdate": "1970-01-01T01:00:00.000Z",
 *          "_id": "622b12d8fffd599f550c6430",
 *          "firstName": "Ludovic",
 *          "lastName": "Flament",
 *          "password": "$2b$10$XGeGC.PO.VSwTt31J7XO3.1S.Vs1cYgF5q4lfWPq8L.ECyNIywyLm",
 *          "email": "ludovic.flament@orange.com",
 *          "apiKey": "2cfeea20-bcc7-4ca7-81d9-2feab39d5a7c",
 *          "creationDate": "2022-03-11T09:14:00.886Z",
 *          "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbl9pZCI6IjYyMmIxMmQ4ZmZmZDU5OWY1NTBjNjQzMCIsImVtYWlsIjoiYnJpZ2l0dGUudmlhdG9uZ3J1bGVAb3JhbmdlLmJvbSIsImlhdCI6MTY0Njk5MDA0MCwiZXhwIjoxNjQ2OTkwNjQwfQ.dzax9_KzoLZ0ra5rLuFjD9ehhyhX7svaYTSyaoShpIE",
 *          "__v": 0
 *      },
 *      {
 *          "lastUpdate": "1970-01-01T01:00:00.000Z",
 *          "_id": "622b12d8fffd599f550c6430",
 *          "firstName": "Ludovic",
 *          "lastName": "Flament",
 *          "password": "$2b$10$XGeGC.PO.VSwTt31J7XO3.1S.Vs1cYgF5q4lfWPq8L.ECyNIywyLm",
 *          "email": "ludovic.flament@orange.com",
 *          "apiKey": "2cfeea20-bcc7-4ca7-81d9-2feab39d5a7c",
 *          "creationDate": "2022-03-11T09:14:00.886Z",
 *          "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbl9pZCI6IjYyMmIxMmQ4ZmZmZDU5OWY1NTBjNjQzMCIsImVtYWlsIjoiYnJpZ2l0dGUudmlhdG9uZ3J1bGVAb3JhbmdlLmJvbSIsImlhdCI6MTY0Njk5MDA0MCwiZXhwIjoxNjQ2OTkwNjQwfQ.dzax9_KzoLZ0ra5rLuFjD9ehhyhX7svaYTSyaoShpIE",
 *          "__v": 0
 *      }
 *    ]
 * 
 * @apiError (2xx) {204} NoContent No content found
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/", verifyToken, async (req, res) => {
  const page = req.query.page || defaultNbPage;
  AdminController.getAllAdministrators(req.user.admin_id, function(status, response) {
    res.status(status).send(response);
  }, page);
});


/**
 * @api {get}/admin/email/{email} Get by email
 * @apiName GetAdminByEmail
 * @apiGroup Admin
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token
 * 
 * @apiSuccess {ObjectId} _id Admin unique identifier
 * @apiSuccess {string} firstName Admin firstName
 * @apiSuccess {string} lastName Admin lastName
 * @apiSuccess {email} email Admin email
 * @apiSuccess {string} apiKey Admin api key
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "_id": "622b12d8fffd599f550c6430",
 *       "firstName": "Ludovic",
 *       "lastName": "Flament",
 *       "email": "ludovic.flament@orange.com",
 *       "apiKey": "2cfeea20-bcc7-4ca7-81d9-2feab39d5a7c",
 *     }
 * 
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {404} NotFound Invalid Email
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/email/:email", verifyToken, (req, res) => {
  const email = req.params.email;

  AdminController.getAdministratorsByEmail(req.user.admin_id, email, function(status, response) {
    res.status(status).send(response);
  });
});


/**
 * @api {get}/admin/id/{id} Get by id
 * @apiName GetAdminById
 * @apiGroup Admin
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token
 * 
 * @apiSuccess {ObjectId} _id Admin unique identifier
 * @apiSuccess {string} firstName Admin firstName
 * @apiSuccess {string} lastName Admin lastName
 * @apiSuccess {email} email Admin email
 * @apiSuccess {string} apiKey Admin api key
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "_id": "622b12d8fffd599f550c6430",
 *       "firstName": "Ludovic",
 *       "lastName": "Flament",
 *       "email": "ludovic.flament@orange.com",
 *       "apiKey": "2cfeea20-bcc7-4ca7-81d9-2feab39d5a7c",
 *     }
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {404} NotFound Invalid id
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/id/:id", verifyToken, (req, res) => {
  const id = req.params.id;

  AdminController.getAdministratorsById(req.user.admin_id, id, function(status, response) {
    res.status(status).send(response);
  });
});

///////////////////////////////////////////////////////////////////
//  POST ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {post}/admin Create
 * @apiName SaveAdmin
 * @apiGroup Admin
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token
 * 
 * @apiBody {string} firstName  
 * @apiBody {string} lastName 
 * @apiBody {string} password
 * @apiBody {email} email exemple@mail.com
 * 
 * @apiSuccess {ObjectId} _id Admin unique identifier
 * @apiSuccess {string} firstName Admin firstName
 * @apiSuccess {string} lastName Admin lastName
 * @apiSuccess {email} email Admin email
 * @apiSuccess {string} apiKey Api key for this admin
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "_id": "622b12d8fffd599f550c6430",
 *       "firstName": "Ludovic",
 *       "lastName": "Flament",
 *       "email": "ludovic.flament@orange.com",
 *       "apiKey": "2cfeea20-bcc7-4ca7-81d9-2feab39d5a7c",
 *     }
 * 
 * @apiError {400} InvalidBody Body is missing, see api doc for more information
 * @apiError {400} InvalidField Validator message
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {401} InvalidQuotas Invalid quotas, retry later or upgrade your account
 * @apiError {409} Conflict Duplicated Email
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.post("/", verifyToken, async (req, res) => {
  AdminController.saveAdministrator(req.user.admin_id, req.body, function(status, response) {
    res.status(status).send(response);
  });
});

///////////////////////////////////////////////////////////////////
// UPDATE ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {put}/admin/id/{id} Update by id
 * @apiName UpdateAdminById
 * @apiGroup Admin
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token
 * 
 * @apiBody {string} firstName John (Optional) 
 * @apiBody {string} lastName Doe (Optional) 
 * @apiBody {string} password azerty13 (Optional) 
 * @apiBody {email} email exemple@mail.com (Optional) 
 * 
 * @apiSuccess {ObjectId} _id Admin unique identifier
 * @apiSuccess {string} firstName Admin firstName
 * @apiSuccess {string} lastName Admin lastName
 * @apiSuccess {email} email Admin email
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "_id": "622b12d8fffd599f550c6430",
 *       "firstName": "Ludovic",
 *       "lastName": "Flament",
 *       "email": "ludovic.flament@orange.com",
 *     }
 * 
 * @apiError {400} InvalidBody Body is missing, see api doc for more information
 * @apiError {400} InvalidId Invalid id
 * @apiError {400} InvalidField Validator message
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {404} InvalidUpdate Nothing was updated
 * @apiError {409} Conflict Duplicated Email
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.put("/id/:id", verifyToken, (req, res) => {
  const id = req.params.id;

  AdminController.updateAdministratorById(req.user.admin_id, req.body, id, function(status, response) {
    res.status(status).send(response);
  });  
});

///////////////////////////////////////////////////////////////////
//  DELETE ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {delete}/admin/id/{id} Delete by id
 * @apiName DeleteAdminById
 * @apiGroup Admin
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Token
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "Delete success"
 *     }
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.delete("/id/:id", verifyToken, (req, res) => {
  const id = req.params.id;

  AdminController.deleteAdministratorById(req.user.admin_id, id, function(status, response) {
    res.status(status).send(response);
  });
});

module.exports = router;
