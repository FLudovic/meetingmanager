// Express routing
const express = require("express");
const router = express.Router();

// Client controller manage routes functions and database requests
const { LogController } = require("../controllers/log-controller");

// Log
const { logger } = require("../common/shared");
const sm = require("../configs/status-messages");
const utils = require("nodemon/lib/utils");

///////////////////////////////////////////////////////////////////
//  GET ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {get}/logs Get last 100 logs
 * @apiName Logs
 * @apiGroup Logs
 * 
 * @apiSuccess {date} date Log date
 * @apiSuccess {string} status Log status (Info, Neutral, Warning, Error)
 * @apiSuccess {string} operation log details 
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "date": "Thu, 10 Mar 2022 19:56:45 GMT",
 *       "status": "[WARNING]",
 *       "operation": "621d34f4c6ad768c4c9894c0  --> /saveAdmin > duplicated email",
 *     }
 * 
 */
router.get("/", (req, res) => {
  LogController.getLogs(function(status, response) {
    res.status(status).send(response);
  });
});

module.exports = router;
