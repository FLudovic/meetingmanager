// Express routing
const express = require("express");
const router  = express.Router();

// Manage session
const { verifyToken } = require("../common/auth");

const { defaultNbPage } = require('../common/shared');

// Admin controller manage routes functions and database requests
const { OrganizationController } = require("../controllers/organization-controller");
   
///////////////////////////////////////////////////////////////////
//  GET ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {get}/organization Get all
 * @apiName GetOrganizations
 * @apiGroup Organization
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {ObjectId} _id Organization unique identifier
 * @apiSuccess {string} name Organization name
 * @apiSuccess {string} address Organization address
 * @apiSuccess {number} employeeNumber Employee number (Optional between 0-100.000)
 * @apiSuccess {ObjectId} _id Admin unique identifier which created this organization
 * @apiSuccess {date} creationDate Organization creation date
 * @apiSuccess {string} logoPath Logo path to Dropbox image (Optional)
 * @apiSuccess {int} __v Version
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *      {
 *        "_id": "622cb0e078e13498889574fc",
 *        "name": "Louis Vuitton",
 *        "address": "Quelque part",
 *        "employeeNumber": 100,
 *        "idAdmin": "622cb01678e13498889574f5",
 *        "date": "2022-03-12T14:40:32.549Z",
 *        "logoPath": "https://www.dropbox.com/s/30u1bg1zrfxetip/16d6325b-3012-4cdc-b676-54bd03a2819a.png?dl=0",
 *        "__v": 0
 *      },
 *      {
 *        "_id": "622cb0e078e13498889574fc",
 *        "name": "Gucci",
 *        "address": "Vers la bas",
 *        "employeeNumber": 101,
 *        "idAdmin": "622cb01678e13498889574f5",
 *        "date": "2022-03-12T14:40:32.549Z",
 *        "logoPath": "https://www.dropbox.com/s/30u1bg1zrfxetip/16d6325b-3012-4cdc-b676-54bd03a2819a.png?dl=0",
 *        "__v": 0
 *      },
 *    ]
 * 
 * @apiError (2xx) {204} NoContent No content found
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} UnauthorizedSuperAdmin You're not Ludovic/Christian/Guillaume or 49.3
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/", verifyToken, (req, res) => {
  const page = req.query.page || defaultNbPage;
  OrganizationController.getAllOrganizations(req.user.admin_id, function(status, response) {
    res.status(status).send(response);
  }, page);
});

/**
 * @api {get}/organization/id/{id} Get by id
 * @apiName GetOrganizationsById
 * @apiGroup Organization
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {ObjectId} _id Organization unique identifier
 * @apiSuccess {string} name Organization name
 * @apiSuccess {string} address Organization address
 * @apiSuccess {number} employeeNumber Employee number (Optional between 0-100.000)
 * @apiSuccess {date} creationDate Organization creation date
 * @apiSuccess {string} logoPath Logo path to Dropbox image (Optional)
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *      {
 *        "_id": "622cb0e078e13498889574fc",
 *        "name": "Louis Vuitton",
 *        "address": "Quelque part",
 *        "employeeNumber": 100,
 *        "date": "2022-03-12T14:40:32.549Z",
 *        "logoPath": "https://www.dropbox.com/s/30u1bg1zrfxetip/16d6325b-3012-4cdc-b676-54bd03a2819a.png?dl=0",
 *      },
 *    ]
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {404} NotFound Invalid id
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/id/:id", verifyToken, (req, res) => {
  const id = req.params.id;

  OrganizationController.getOrganizationsById(req.user.admin_id, id, function(status, response) {
    res.status(status).send(response);
  });  
});


/**
 * @api {get}/organization/admin Get by admin id (Provided by the token)
 * @apiName GetOrganizationsByAdminId
 * @apiGroup Organization
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {ObjectId} _id Organization unique identifier
 * @apiSuccess {string} name Organization name
 * @apiSuccess {string} address Organization address
 * @apiSuccess {number} employeeNumber Employee number (Optional between 0-100.000)
 * @apiSuccess {date} creationDate Organization creation date
 * @apiSuccess {string} logoPath Logo path to Dropbox image (Optional)
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *      {
 *        "_id": "622cb0e078e13498889574fc",
 *        "name": "Louis Vuitton",
 *        "address": "Quelque part",
 *        "employeeNumber": 100,
 *        "date": "2022-03-12T14:40:32.549Z",
 *        "logoPath": "https://www.dropbox.com/s/30u1bg1zrfxetip/16d6325b-3012-4cdc-b676-54bd03a2819a.png?dl=0",
 *      },
 *    ]
 * 
 * @apiError {400} InvalidId Invalid id format
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {404} NotFound Invalid id
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/admin", verifyToken, (req, res) => {
  OrganizationController.getOrganizationsByAdminId(req.user.admin_id, function(status, response) {
    res.status(status).send(response);
  });  
});

///////////////////////////////////////////////////////////////////
//  POST ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {post}/organization Create
 * @apiName SaveOrganization
 * @apiGroup Organization
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiBody {string} name 
 * @apiBody {string} address
 * @apiBody {number} employeeNumber Employee number (Optional between 0-100.000)
 * @apiBody {base64} image Base64 image only
 * 
 * @apiSuccess {ObjectId} _id Organization unique identifier
 * @apiSuccess {string} name Organization name
 * @apiSuccess {string} address Organization address
 * @apiSuccess {number} employeeNumber Employee number (Optional between 0-100.000)
 * @apiSuccess {date} creationDate Organization creation date
 * @apiSuccess {string} logoPath Logo path to Dropbox image (Optional)
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *      {
 *        "_id": "622cb0e078e13498889574fc",
 *        "name": "Louis Vuitton",
 *        "address": "Quelque part",
 *        "employeeNumber": 100,
 *        "date": "2022-03-12T14:40:32.549Z",
 *        "logoPath": "https://www.dropbox.com/s/30u1bg1zrfxetip/16d6325b-3012-4cdc-b676-54bd03a2819a.png?dl=0",
 *      },
 *    ]
 * 
 * @apiError {400} InvalidBody Body is missing, see api doc for more information
 * @apiError {400} InvalidImage Invalid image or logo format
 * @apiError {400} InvalidField Validator message
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.post("/", verifyToken, (req, res) => {
  OrganizationController.saveOrganization(req.user.admin_id, req.body, function(status, response) {
    res.status(status).send(response);
  });  
});

///////////////////////////////////////////////////////////////////
//  UPDATE ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {put}/organization/id/{id} Update by id
 * @apiName UpdateOrganization
 * @apiGroup Organization
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiBody {string} name 
 * @apiBody {string} address
 * @apiBody {number} employeeNumber Employee number (Optional between 0-100.000)
 * @apiBody {base64} image Base64 image only
 * 
 * @apiSuccess {ObjectId} _id Organization unique identifier
 * @apiSuccess {string} name Organization name
 * @apiSuccess {string} address Organization address
 * @apiSuccess {number} employeeNumber Employee number (Optional between 0-100.000)
 * @apiSuccess {date} creationDate Organization creation date
 * @apiSuccess {string} logoPath Logo path to Dropbox image (Optional)
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *      {
 *        "_id": "622cb0e078e13498889574fc",
 *        "name": "Louis Vuitton",
 *        "address": "Quelque part",
 *        "employeeNumber": 100,
 *        "date": "2022-03-12T14:40:32.549Z",
 *        "logoPath": "https://www.dropbox.com/s/30u1bg1zrfxetip/16d6325b-3012-4cdc-b676-54bd03a2819a.png?dl=0",
 *      },
 *    ]
 * 
 * @apiError {400} InvalidBody Body is missing, see api doc for more information
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {400} InvalidImage Invalid image or logo format
 * @apiError {400} InvalidField Validator message
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {401} InvalidPrivilege hop, hop, hop, you don\'t have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨'
 * @apiError {404} InvalidUpdate Nothing was updated
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.put("/id/:id", verifyToken, async (req, res) => {
  const id = req.params.id;

  OrganizationController.updateOrganizationById(req.user.admin_id, req.body, id, function(status, response) {
    res.status(status).send(response);
  });
});


///////////////////////////////////////////////////////////////////
//  DELETE ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {delete}/organization/id/{id} Delete by id
 * @apiName DeleteOrganizationById
 * @apiGroup Organization
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "Delete success"
 *     }
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.delete("/id/:id", verifyToken, async (req, res) => {
  const id = req.params.id;

  OrganizationController.deleteOrganizationById(req.user.admin_id, id, function(status, response) {
    res.status(status).send(response);
  });
});

module.exports = router;
