// Express routing
const express = require("express");
const router = express.Router();

// Import jsonwebtoken to manage token
const { verifyToken } = require("../common/auth");

const { defaultNbPage } = require('../common/shared');

// Client controller manage routes functions and database requests
const { ClientController } = require("../controllers/client-controller");

///////////////////////////////////////////////////////////////////
//  GET ROUTES METHODS
///////////////////////////////////////////////////////////////////


/**
 * @api {get}/client Get all
 * @apiName GetAllClients
 * @apiGroup Client
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {ObjectId} _id Client unique identifier
 * @apiSuccess {string} firstName Client firstName
 * @apiSuccess {string} lastName Client lastName
 * @apiSuccess {email} email Client email
 * @apiSuccess {phone} string Client phone
 * @apiSuccess {ObjectId} idOrganization Unique identifier of related organization 
 * @apiSuccess {date} creationDate Client creation date
 * @apiSuccess {int} __v Version
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "_id": "622cd8343adf5bd4069bca5b",
 *         "firstName": "Premier",
 *         "lastName": "Client",
 *         "email": "premieeeeer@client.com",
 *         "phone": "0494049404",
 *         "idOrganization": "622cd8333adf5bd4069bca56",
 *         "date": "2022-03-12T17:28:20.286Z",
 *         "__v": 0
 *       },
 *       {
 *        "_id": "622cd8343adf5bd4069bca5e",
 *        "firstName": "Deuxième",
 *        "lastName": "Client",
 *        "email": "deuxieeeme@client.com",
 *        "phone": "0494049404",
 *        "idOrganization": "622cd8333adf5bd4069bca56",
 *        "date": "2022-03-12T17:28:20.593Z",
 *        "__v": 0
 *       },
 *    ]
 * 
 * @apiError (2xx) {204} NoContent No content found
 * @apiError {401} UnauthorizedSuperAdmin You're not super admin
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/", verifyToken, (req, res) => {
  const page = req.query.page || defaultNbPage;
  // Filter on last inserted
  ClientController.getClients(req.user.admin_id, function(status, response) {
    res.status(status).send(response);
  }, page);
});

/**
 * @api {get}/client/email/{email} Get by email
 * @apiName GetClientByEmail
 * @apiGroup Client
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {ObjectId} _id Client unique identifier
 * @apiSuccess {string} firstName Client firstName
 * @apiSuccess {string} lastName Client lastName
 * @apiSuccess {email} email Client email
 * @apiSuccess {phone} string Client phone
 * @apiSuccess {date} creationDate Client creation date
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "_id": "622cd8343adf5bd4069bca5b",
 *         "firstName": "Premier",
 *         "lastName": "Client",
 *         "email": "premieeeeer@client.com",
 *         "phone": "0494049404",
 *         "date": "2022-03-12T17:28:20.286Z",
 *       },
 *    ]
 * 
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {404} InvalidEmail Invalid email
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/email/:email", verifyToken, (req, res) => {
  ClientController.getClientByEmail(req.user.admin_id, req.params.email, function(status, response) {
    res.status(status).send(response);
  });
});

/**
 * @api {get}/client/id/{id} Get by id
 * @apiName GetClientById
 * @apiGroup Client
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {ObjectId} _id Client unique identifier
 * @apiSuccess {string} firstName Client firstName
 * @apiSuccess {string} lastName Client lastName
 * @apiSuccess {email} email Client email
 * @apiSuccess {phone} string Client phone
 * @apiSuccess {date} creationDate Client creation date
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "_id": "622cd8343adf5bd4069bca5b",
 *         "firstName": "Premier",
 *         "lastName": "Client",
 *         "email": "premieeeeer@client.com",
 *         "phone": "0494049404",
 *         "date": "2022-03-12T17:28:20.286Z",
 *       },
 *    ]
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {404} InvalidId Invalid id
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/id/:id", verifyToken, (req, res) => {
  ClientController.getClientById(req.user.admin_id, req.params.id, function(status, response) {
    res.status(status).send(response);
  });
});

///////////////////////////////////////////////////////////////////
//  POST ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {post}/client Create
 * @apiName SaveClient
 * @apiGroup Client
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token
 * 
 * @apiBody {string} firstName
 * @apiBody {string} lastName
 * @apiBody {string} phone
 * @apiBody {email} email
 * @apiBody {string} idOrganization Organization id related to the client
 * 
 * @apiSuccess {string} firstName Client firstName
 * @apiSuccess {string} lastName Client lastName
 * @apiSuccess {email} email Client email
 * @apiSuccess {string} phone Client phone
 * @apiSuccess {string} idOrganization Unique identifier of organization
 * @apiSuccess {ObjectId} _id Client unique identifier
 * @apiSuccess {date} creationDate Admin creation date
 * @apiSuccess {int} __v Version
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *       {
 *           "firstName": "John",
 *           "lastName": "Doe",
 *           "email": "johnDoe@gmail.com",
 *           "phone": "0666666666",
 *           "idOrganization": "621d256ff2396782ed777f96",
 *           "_id": "622fbfe816e60179eb5d070c",
 *           "date": "2022-03-14T22:21:28.826Z",
 *           "__v": 0
 *       }
 * 
 * @apiError {400} InvalidOrganizationId Invalid organization id format
 * @apiError {400} InvalidBody Invalid body
 * @apiError {400} InvalidField Validator message
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨". Only the admin who created the organization can add a client to it
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {409} Conflict Duplicated email
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.post("/", verifyToken, async (req, res) => {
  ClientController.saveClient(req.user.admin_id, req.body, function(status, response) {
    res.status(status).send(response);
  });
});

///////////////////////////////////////////////////////////////////
//  UPDATE ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {put}/client/id/{id} Update by id
 * @apiName UpdateClient
 * @apiGroup Client
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token
 * 
 * @apiBody {string} firstName
 * @apiBody {string} lastName
 * @apiBody {string} phone
 * @apiBody {email} email
 * @apiBody {string} idOrganization Organization id related to the client
 * 
 * @apiSuccess {ObjectId} _id Client unique identifier
 * @apiSuccess {string} firstName Client firstName
 * @apiSuccess {string} lastName Client lastName
 * @apiSuccess {email} email Client email
 * @apiSuccess {string} phone Client phone
 * @apiSuccess {date} creationDate Admin creation date
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *       {
 *           "firstName": "Jane",
 *           "lastName": "Doe",
 *           "email": "johnDoe@gmail.com",
 *           "phone": "0666666666",
 *           "_id": "622fbfe816e60179eb5d070c",
 *           "date": "2022-03-14T22:21:28.826Z",
 *       }
 * 
 * @apiError {400} InvalidBody Invalid body
 * @apiError {400} InvalidField Validator message
 * @apiError {400} InvalidClientIdFormat Invalid client id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨". Only the admin who created the organization can add a client to it
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {404} InvalidUpdate Nothing was updated
 * @apiError {409} Conflict Duplicated email
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.put("/id/:id", verifyToken, (req, res) => {
  ClientController.updateClientById(req.user.admin_id, req.body, req.params.id, function(status, response) {
    res.status(status).send(response);
  });
});

///////////////////////////////////////////////////////////////////
//  DELETE ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {delete}/client/id/{id} Delete by id
 * @apiName DeleteClient
 * @apiGroup Client
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token
 * 
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "Delete success"
 *     }
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.delete("/id/:id", verifyToken, (req, res) => {
  ClientController.deleteClientById(req.user.admin_id, req.params.id, function(status, response) {
    res.status(status).send(response);
  });
});

module.exports = router;
