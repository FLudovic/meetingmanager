// Express routing
const express = require("express");
const router  = express.Router();

// Connection controller manage routes functions and database requests
const { ConnectionController } = require('../controllers/connection-controller');

/**
 * @api {post}/connection/login Request login
 * @apiName Login
 * @apiGroup Connection
 * 
 * @apiParam {email} email Admin email 
 * @apiParam {string} ApiKey Admin api key 
 * 
 * @apiSuccess {string} token Admin Token
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "token": "dferuxsfsfjrehfurr11sdzrth47th854zsdfgSFDFfghrgfrgSW2sf2sfSDsfsf574542sdqsfkeff1geGFedgfkgvklfdhjkhvxjvkz1dfdg21bjk1sfdsfhjhfffuurtyizedujbcbfdgvjhfgyurfglfk"
 *     }
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidCredential Invalid Credentials
 * @apiError {404} InvalidInput All input are required
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.post("/login", async (req, res) => {
  ConnectionController.connectionLogin(req.body, function(status, response) {
    res.status(status).send(response);
  });
});

module.exports = router;
