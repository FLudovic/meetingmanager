// Express routing
const express = require("express");
const router = express.Router();

// Manage session
const { verifyToken } = require("../common/auth");

// Client picture controller manage routes functions and database requests
const { ClientPictureController } = require('../controllers/client-picture-controller');

///////////////////////////////////////////////////////////////////
//  GET ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {get}/clientPicture Get all
 * @apiName GetPictures
 * @apiGroup ClientPictures
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {ObjectId} _id ClientPictures unique identifier
 * @apiSuccess {ObjectId} admin unique id
 * @apiSuccess {ObjectId} client unique id
 * @apiSuccess {date} creationDate client picture creation date
 * @apiSuccess {string} path to the client picture in dropbox
 * @apiSuccess {int} __v Version
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    [
 *      {
 *        "_id": "623452994ddc0383d393cdaa",
 *        "idAdmin": "621d34f4c6ad768c4c9894c0",
 *        "idClient": "62344a8c4ffe44f77402d8f9",
 *        "date": "2022-03-18T09:36:25.647Z",
 *        "picturePath": "https://www.dropbox.com/s/7j3c0m3pjvlb681/0129da17-dfb4-420f-a73b-37673ea8c1dc.png?dl=0",
 *        "__v": 0
 *      },
 *      {
 *        "_id": "623452074ddc0383d393cda6",
 *        "idAdmin": "621d34f4c6ad768c4c9894c0",
 *        "idClient": "62344a8c4ffe44f77402d8f9",
 *        "date": "2022-03-18T09:33:59.643Z",
 *        "picturePath": "https://www.dropbox.com/s/d924iembpb6qg1w/f87d432b-d421-4ea9-a8d5-5bcf80a6b0f5.png?dl=0",
 *        "__v": 0
 *      }
 *    ]
 * 
 * @apiError (2xx) {204} NoContent No content found
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} UnauthorizedSuperAdmin You're not super admin
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/", verifyToken, (req, res) => {
  ClientPictureController.getPictures(req.user.admin_id, function(status, response) {
    res.status(status).send(response);
  });
});

/**
 * @api {get}/clientPicture/id/{id} Get by id
 * @apiName GetClientPictureById
 * @apiGroup ClientPicture
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {ObjectId} _id ClientPictures unique identifier
 * @apiSuccess {ObjectId} admin unique id
 * @apiSuccess {ObjectId} client unique id
 * @apiSuccess {date} creationDate client picture creation date
 * @apiSuccess {string} path to the client picture in dropbox
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    [
 *      {
 *          "_id": "623452994ddc0383d393cdaa",
 *          "idAdmin": "621d34f4c6ad768c4c9894c0",
 *          "idClient": "62344a8c4ffe44f77402d8f9",
 *          "date": "2022-03-18T09:36:25.647Z",
 *          "picturePath": "https://www.dropbox.com/s/7j3c0m3pjvlb681/0129da17-dfb4-420f-a73b-37673ea8c1dc.png?dl=0",
 *      }
 *    ]
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {404} InvalidIdValue Invalid id value
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/id/:id", verifyToken, (req, res) => {
  const id = req.params.id;

  ClientPictureController.getPicturesById(req.user.admin_id, id, function(status, response) {
    res.status(status).send(response);
  });
});

/**
 * @api {get}/clientPicture/client/id/{id} Get by client id
 * @apiName GetClientPictureByClientId
 * @apiGroup ClientPicture
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {ObjectId} _id ClientPictures unique identifier
 * @apiSuccess {ObjectId} admin unique id
 * @apiSuccess {ObjectId} client unique id
 * @apiSuccess {date} creationDate client picture creation date
 * @apiSuccess {string} path to the client picture in dropbox
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    [
 *      {
 *          "_id": "623452994ddc0383d393cdaa",
 *          "idAdmin": "621d34f4c6ad768c4c9894c0",
 *          "idClient": "62344a8c4ffe44f77402d8f9",
 *          "date": "2022-03-18T09:36:25.647Z",
 *          "picturePath": "https://www.dropbox.com/s/7j3c0m3pjvlb681/0129da17-dfb4-420f-a73b-37673ea8c1dc.png?dl=0",
 *      },
 *      {
 *          "_id": "623452994ddc0383d393cdbb",
 *          "idAdmin": "621d34f4c6ad768c4c9894c0",
 *          "idClient": "62344a8c4ffe44f77402d8f7",
 *          "date": "2022-03-18T09:36:50.647Z",
 *          "picturePath": "https://www.dropbox.com/s/7j3c0m3pjvlb681/0129da54-dfb4-420f-a73b-37673ea8c1dc.png?dl=0",
 *      }
 *    ]
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {404} InvalidIdValue Invalid id value
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/client/id/:id", verifyToken, (req, res) => {
  const id = req.params.id;

  ClientPictureController.getPicturesByClientId(req.user.admin_id, id, function(status, response) {
    res.status(status).send(response);
  });
});

///////////////////////////////////////////////////////////////////
//  POST ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {post}/clientPicture/ Create
 * @apiName SaveClientPicture
 * @apiGroup ClientPicture
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiBody {ObjectId} idClient
 * @apiBody {string} picture picture (Base64)
 * 
 * @apiSuccess {ObjectId} _id ClientPictures unique identifier
 * @apiSuccess {ObjectId} admin unique id
 * @apiSuccess {ObjectId} client unique id
 * @apiSuccess {date} creationDate client picture creation date
 * @apiSuccess {string} path to the client picture in dropbox
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    [
 *      {
 *          "_id": "623452994ddc0383d393cdbb",
 *          "idAdmin": "621d34f4c6ad768c4c9894c0",
 *          "idClient": "62344a8c4ffe44f77402d8f7",
 *          "date": "2022-03-18T09:36:50.647Z",
 *          "picturePath": "https://www.dropbox.com/s/7j3c0m3pjvlb681/0129da54-dfb4-420f-a73b-37673ea8c1dc.png?dl=0",
 *      }
 *    ]
 * 
 * @apiError {400} InvalidBody Body is missing, see api doc for more information
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.post("/", verifyToken, async (req, res) => {
  ClientPictureController.savePicture(req.user.admin_id, req.body, function(status, response) {
    res.status(status).send(response);
  });
});

///////////////////////////////////////////////////////////////////
//  UPDATE ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {put}/clientPicture/id/{id} Update by id
 * @apiName UpdateClientPictureById
 * @apiGroup ClientPicture
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token
 * 
 * @apiError (501 Not Implemented) {NotImplemented} Hum this will be done in a future update ( ͡° ͜ʖ├┬┴┬
 */
router.put("/id/:id", verifyToken, async (req, res) => {
  const id = req.params.id;

  ClientPictureController.updatePictureById(req.user.admin_id, req.body, id, function(status, response) {
    res.status(status).send(response);
  });
});

///////////////////////////////////////////////////////////////////
//  DELETE ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {delete}/clientPicture/client/id/{id} Delete by client id
 * @apiName DeleteClientPictureByClientId
 * @apiGroup ClientPicture
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Token
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "Delete success"
 *     }
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.delete("/client/id/:id", verifyToken, async (req, res) => {
  const id = req.params.id;

  ClientPictureController.deletePicturesByClientId(req.user.admin_id, id, function(status, response) {
    res.status(status).send(response);
  });
});

/**
 * @api {delete}/clientPicture/id/{id} Delete by id
 * @apiName DeleteClientPictureById
 * @apiGroup ClientPicture
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Token
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "Delete success"
 *     }
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.delete("/id/:id", verifyToken, async (req, res) => {
  const id = req.params.id;

  ClientPictureController.deletePictureById(req.user.admin_id, id, function(status, response) {
    res.status(status).send(response);
  });
});


module.exports = router;
