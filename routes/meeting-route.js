// Express routing
const express = require("express");
const router  = express.Router();

// Manage session
const { verifyToken } = require("../common/auth");

// Meeting picture controller manage routes functions and database requests
const { MeetingController } = require('../controllers/meeting-controller');

const { defaultNbPage } = require('../common/shared');

///////////////////////////////////////////////////////////////////
//  GET ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {get}/meeting Get all
 * @apiName GetMeeting
 * @apiGroup Meeting
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {ObjectId} _id Meeting unique identifier
 * @apiSuccess {string} content Meeting description
 * @apiSuccess {date} meetingDate Meeting date
 * @apiSuccess {string} place Meeting place (Optional)
 * @apiSuccess {ObjectId} _id Client unique identifier
 * @apiSuccess {date} creationDate Meeting creation date
 * @apiSuccess {int} __v Version
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    [
 *   {
 *       "_id": "622cd6813adf5bd4069bca50",
 *       "content": "Second meeting",
 *       "meetingDate": "2020-10-20T00:00:00.000Z",
 *       "place": "France",
 *       "idClient": "622cd6813adf5bd4069bca46",
 *       "date": "2022-03-12T17:21:05.378Z",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "622cd6813adf5bd4069bca4e",
 *       "content": "Premier meeting",
 *       "meetingDate": "2020-10-20T00:00:00.000Z",
 *       "place": "France",
 *       "idClient": "622cd6813adf5bd4069bca46",
 *       "date": "2022-03-12T17:21:05.326Z",
 *       "__v": 0
 *   }
 *    ]
 * 
 * @apiError (2xx) {204} NoContent No content found
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} UnauthorizedSuperAdmin You're not super admin
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/", verifyToken, (req, res) => {
    const page = req.query.page || defaultNbPage;
    MeetingController.getAllMeetings(req.user.admin_id, function(status, response) {
        res.status(status).send(response);
    }, page);
});

/**
 * @api {get}/meeting/id/{id} Get by id
 * @apiName GetMeetingById
 * @apiGroup Meeting
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {ObjectId} _id Meeting unique identifier
 * @apiSuccess {string} content Meeting description
 * @apiSuccess {date} meetingDate Meeting date
 * @apiSuccess {string} place Meeting place (Optional)
 * @apiSuccess {date} creationDate Meeting creation date
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    [
 *      {
 *          "_id": "622cd6813adf5bd4069bca50",
 *          "content": "Second meeting",
 *          "meetingDate": "2020-10-20T00:00:00.000Z",
 *          "place": "France",
 *          "date": "2022-03-12T17:21:05.378Z",
 *      }
 *    ]
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {404} InvalidIdValue Invalid id value
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/id/:id", verifyToken, (req, res) => {
    const id = req.params.id;

    MeetingController.getMeetingsById(req.user.admin_id, id, function(status, response) {
        res.status(status).send(response);
    });
});


/**
 * @api {get}/meeting/client/id/{id} Get by client id
 * @apiName GetMeetingByClientId
 * @apiGroup Meeting
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiSuccess {ObjectId} _id Meeting unique identifier
 * @apiSuccess {string} content Meeting description
 * @apiSuccess {date} meetingDate Meeting date
 * @apiSuccess {string} place Meeting place (Optional)
 * @apiSuccess {date} creationDate Meeting creation date
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    [
 *      {
 *          "_id": "622cd6813adf5bd4069bca50",
 *          "content": "Second meeting",
 *          "meetingDate": "2020-10-20T00:00:00.000Z",
 *          "place": "France",
 *          "date": "2022-03-12T17:21:05.378Z",
 *      }
 *    ]
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError {404} InvalidIdValue Invalid id value
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.get("/client/id/:id", verifyToken, (req, res) => {
    const id = req.params.id;

    MeetingController.getMeetingsByClientId(req.user.admin_id, id, function(status, response) {
        res.status(status).send(response);
    });    
});



///////////////////////////////////////////////////////////////////
//  POST ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {post}/meeting/ Create
 * @apiName SaveMeeting
 * @apiGroup Meeting
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token 
 * 
 * @apiBody {string} content
 * @apiBody {string} meeting 
 * @apiBody {string} place (Optional)
 * @apiBody {ObjectId} idClient
 * 
 * @apiSuccess {ObjectId} _id Meeting unique identifier
 * @apiSuccess {string} content Meeting description
 * @apiSuccess {date} meetingDate Meeting date
 * @apiSuccess {string} place Meeting place (Optional)
 * @apiSuccess {date} creationDate Meeting creation date
 * 
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    [
 *      {
 *          "_id": "622cd6813adf5bd4069bca50",
 *          "content": "Entretien d'embauche",
 *          "meetingDate": "2020-10-20",
 *          "place": "Aix en Provence",
 *          "date": "2022-03-12T17:21:05.378Z",
 *      }
 *    ]
 * 
 * @apiError {400} InvalidBody Body is missing, see api doc for more information
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.post("/", verifyToken, (req, res) => {
    MeetingController.saveMeeting(req.user.admin_id, req.body, function(status, response) {
        res.status(status).send(response);
    });   
});

///////////////////////////////////////////////////////////////////
//  UPDATE ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {put}/meeting/id/{id} Update by id
 * @apiName UpdateMeetingById
 * @apiGroup Meeting
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Admin Token
 * 
 * @apiBody {string} content
 * @apiBody {string} meeting 
 * @apiBody {ObjectId} client id
 * @apiBody {string} place (Optional)
 * 
 * @apiSuccess {ObjectId} _id Organization unique identifier
 * @apiSuccess {string} content meeting description
 * @apiSuccess {date} meetingDate meeting date
 * @apiSuccess {string} place Meeting place (Optional)
 * @apiSuccess {date} creationDate Meeting creation date
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "_id": "622cd6813adf5bd4069bca50",
 *          "content": "Entretien d'embauche",
 *          "meetingDate": "2020-10-20",
 *          "place": "Aix en Provence",
 *          "date": "2022-03-12T17:21:05.378Z",
 *     }
 * 
 * @apiError {400} InvalidBody Body is missing, see api doc for more information
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.put("/id/:id", verifyToken, async (req, res) => {
    const id = req.params.id;

    MeetingController.updateMeetingById(req.user.admin_id, req.body, id, function(status, response) {
        res.status(status).send(response);
    });     
});


///////////////////////////////////////////////////////////////////
//  DELETE ROUTES METHODS
///////////////////////////////////////////////////////////////////

/**
 * @api {delete}/meeting/id/{id} Delete by id
 * @apiName DeleteMeetingById
 * @apiGroup Meeting
 * 
 * @apiPermission JWToken (x-access-token)
 * 
 * @apiParam {token} Token
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "Delete success"
 *     }
 * 
 * @apiError {400} InvalidIdFormat Invalid id format
 * @apiError {401} InvalidPrivilege Hop, hop, hop, you don't have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨
 * @apiError {401} Unauthorized A token is required for authentication
 * @apiError {401} TokenExpiredError Your token has expired
 * @apiError {401} InvalidToken Invalid Token
 * @apiError (500 Internal Server Error) {InternalServerError} InternalServerError へ‿(ツ)‿ㄏ
 */
router.delete("/id/:id", verifyToken, async (req, res) => {
    const id = req.params.id;

    MeetingController.deleteMeetingById(req.user.admin_id, id, function(status, response) {
        res.status(status).send(response);
    }); 
});

module.exports = router;
  
