///////////////////////////////////////////////////////////////////
//  API Messages
///////////////////////////////////////////////////////////////////

// Administrators
getAdminsError   = 'Error has occurred while attempting to retrieve administrators';
saveAdminError   = 'Error has occurred while attempting to create administrator';
updateAdminError = 'Error has occurred while attempting to update administrator';
deleteAdminError = 'Error has occurred while attempting to delete administrator';

// Client
getClientsError   = 'Error has occurred while attempting to retrieve clients';
saveClientError   = 'Error has occurred while attempting to create client';
updateClientError = 'Error has occurred while attempting to update client';
deleteClientError = 'Error has occurred while attempting to delete client';

// Organization
getOrganizationsError   = 'Error has occurred while attempting to retrieve organizations';
saveOrganizationError   = 'Error has occurred while attempting to create organization';
updateOrganizationError = 'Error has occurred while attempting to update organization';
deleteOrganizationError = 'Error has occurred while attempting to delete organization';

// Meeting
getMeetingsError   = 'Error has occurred while attempting to retrieve meetings';
saveMeetingError   = 'Error has occurred while attempting to create meeting';
updateMeetingError = 'Error has occurred while attempting to update meeting';
deleteMeetingError = 'Error has occurred while attempting to delete meeting';

// Picture 
getPicturesError   = 'Error has occurred while attempting to retrieve picture';
savePictureError   = 'Error has occurred while attempting to save picture';
deletePictureError = 'Error has occurred while attempting to delete picture';

// Connection
loginError = 'Error has occurred while attempting to login';



// Email
sendError   = 'Error has occurred while attempting to send email';
sendSuccess = 'Email sent: ';

// Global
deleteSuccess         = ' > delete success';
duplicatedEmail       = ' > duplicated email';
noContent             = ' > no content or invalid request';
invalidId             = ' > invalid id';
invalidEmail          = ' > invalid email';
invalidImage          = ' > invalid image or logo format';
invalidQuotas         = ' > invalid quotas, retry later or upgrade your account';
invalidIdOrganization = ' > invalid id organization';
invalidPrivilege      = ' > hop, hop, hop, you don\'t have the permission to do that... ୧( ͡ᵔ 益 ͡ᵔ )୨';
invalidUpdate         = ' > nothing was updated';

invalidBody        = ' > body is missing, see api doc for more';
invalidInputs      = ' > all input are required';
invalidCredentials = ' > invalid Credentials';

updateRefused = ' > update refused try to log with another administrator';
deleteRefused = ' > delete refused try to log with another administrator';


///////////////////////////////////////////////////////////////////
//  logger Messages
///////////////////////////////////////////////////////////////////

// Administrators
getAdmins        = '--> /getAdmins';
getAdminsByEmail = '--> /getAdminsByEmail';
getAdminsById    = '--> /getAdminsById';
saveAdmin        = '--> /createAdmin';
updateAdmin      = '--> /updateAdmin';
deleteAdmin      = '--> /deleteAdmin';

// Client
getClients        = '--> /getClients';
getClientsByEmail = '--> /getClientsByEmail';
getClientsById    = '--> /getClientsById';
saveClient        = '--> /createClient';
updateClient      = '--> /updateClient';
deleteClient      = '--> /deleteClient';

// Organizations
getOrganizations          = '--> /getOrganizations';
getOrganizationsById      = '--> /getOrganizationsById';
getOrganizationsByAdminId = '--> /getOrganizationsByAdminId';
saveOrganization          = '--> /createOrganization';
updateOrganization        = '--> /updateOrganization';
deleteOrganization        = '--> /deleteOrganization';

// Meetings
getMeetings           = '--> /getMeetings';
getMeetingsById       = '--> /getMeetingsById';
getMeetingsByClientId = '--> /getMeetingsByClientId';
saveMeeting           = '--> /createMeeting';
updateMeeting         = '--> /updateMeeting';
deleteMeeting         = '--> /deleteMeeting';

// Pictures
getPictures           = '--> /getPictures';
getPicturesById       = '--> /getPicturesById';
getPicturesByClientId = '--> /getPicturesByClientId';
savePicture           = '--> /createPicture';
deletePicture         = '--> /deletePicture';
deletePicturesByClientId = '--> /deletePicturesByClientId';

// Connection
login = '--> /login';

// Pre request Delete
deleteAdminCascade = 'Error on admin cascade delete';
deleteOrganizationCascade = 'Error on organization cascade delete';
deleteClientCascade = 'Error on client cascade delete';
deleteMeetingCascade = 'Error on meeting cascade delete';
deletePictureCascade = 'Error on picture cascade delete';

deleteAdminCascadeSuccess = 'Delete cascade admins ';
deleteOrganizationCascadeSuccess = 'Delete cascade organizations ';
deleteClientCascadeSuccess = 'Delete cascade clients';
deleteMeetingCascadeSuccess = 'Delete cascade meetings ';
deletePictureCascadeSuccess = 'Delete cascade pictures ';



module.exports = {
    /////////////////////////////////////////
    // API MESSAGES
    /////////////////////////////////////////

    // Administrators
    getAdminsError,
    saveAdminError,
    updateAdminError,
    deleteAdminError,

    // Clients
    getClientsError,
    saveClientError,
    updateClientError,
    deleteClientError,

    // Organizations
    getOrganizationsError,
    saveOrganizationError,
    updateOrganizationError,
    deleteOrganizationError,

    // Meetings
    getMeetingsError,
    saveMeetingError,
    updateMeetingError,
    deleteMeetingError,

    // Picture
    savePictureError,
    deletePictureError,
    getPicturesError,

    // Emails
    sendError,
    sendSuccess,

    // Connections
    loginError,


    // Global Messages
    deleteSuccess,
    duplicatedEmail,
    noContent,
    invalidId,
    invalidIdOrganization,
    invalidBody,
    invalidInputs,
    invalidEmail,
    invalidQuotas,
    invalidImage,
    invalidPrivilege,
    invalidCredentials,
    invalidUpdate,

    updateRefused,
    deleteRefused,


    /////////////////////////////////////////
    // LOGGER MESSAGES
    /////////////////////////////////////////

    // Administrators
    getAdmins,
    getAdminsByEmail,
    getAdminsById,
    saveAdmin,
    updateAdmin,
    deleteAdmin,

    // Clients
    getClients,
    getClientsByEmail,
    getClientsById,
    saveClient,
    updateClient,
    deleteClient,

    // Organizations
    getOrganizations,
    getOrganizationsById,
    getOrganizationsByAdminId,
    saveOrganization,
    updateOrganization,
    deleteOrganization,

    // Meetings
    getMeetings,
    getMeetingsById,
    getMeetingsByClientId,
    saveMeeting,
    updateMeeting,
    deleteMeeting,

    // Pictures
    getPictures,
    getPicturesById,
    getPicturesByClientId,
    savePicture,
    deletePicture,
    deletePicturesByClientId,

    // Connection
    login,

    // Pre request delete
    deleteAdminCascade,
    deleteOrganizationCascade,
    deleteClientCascade,
    deleteMeetingCascade,
    deletePictureCascade,

    deleteAdminCascadeSuccess,
    deleteOrganizationCascadeSuccess,
    deleteClientCascadeSuccess,
    deleteMeetingCascadeSuccess,
    deletePictureCascadeSuccess
};
