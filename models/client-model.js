const mongoose = require('mongoose');
const { EmailValidator, NameValidator, PasswordValidator, PhoneValidator} = require('../common/validators');
const { EmailErrorMessages, NameErrorMessages, PasswordErrorMessages, PhoneErrorMessages } = require('../common/messages');
const { ObjectId } = require('mongodb');

const MeetingSchema = require("../models/meeting-model");
const ClientPictureSchema = require("../models/client-picture-model");

const sm = require("../configs/status-messages");
const {compareDates, logger}  = require("../common/shared");
const c = require('../configs/colors');

const ClientSchema = new mongoose.Schema({
  firstName: {
    type: String,
    validate: {
        validator: NameValidator,
        message: props => NameErrorMessages(props)  
    },
    required: true
  },
  lastName: {
    type: String,
    validate: {
        validator: NameValidator,
        message: props => NameErrorMessages(props)
    },
    required: true
  },
  email: {
    type: String,
    validate: {
        validator: EmailValidator,
        message: props => EmailErrorMessages(props)  
    },
    required: true,
    index: {
      unique: true
    }
  },
  phone: {
    type: String,
    validate: {
      validator: PhoneValidator,
      message: props => PhoneErrorMessages(props)  
    },
    required: true
  },
  age: {
    type: Number,
    min: [0, 'Must be at least 0, got {VALUE}'],
    max: [100, 'You can\'t have more than 100 years antiquity {VALUE}']
  },
  idOrganization: {
    type: ObjectId,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

ClientSchema.pre('deleteMany', async function(next) {
  // This refers to the request, get _id
  const id = this.getQuery().idOrganization;
  // Get from organization id list of clients which own
  await Client.find({idOrganization: id}).then((clientList) => {
    // Transform the client array result in id array
    let idArray = [];
    clientList.forEach((client) => {
      idArray.push(client._id);
    });
    // Delete meetings with organization id
    MeetingSchema.deleteMany({ idClient: { $in: idArray }}).then((res) => {
      console.log("delete on client model success");
      ClientPictureSchema.deleteMany({ idClient: { $in: idArray }}).then((res) => {
        console.log("delete on client model success");
        next();
      }).catch((err) => {
        console.log("delete on client model error" + err);
      });
    }).catch((err) => {
      console.log("delete on client model error" + err);
    });
  }).catch((err) => {
    console.log("delete on client model error" + err);
  });
});

const Client = mongoose.model('clients', ClientSchema);

module.exports = Client;
