const { ObjectId } = require('mongodb');

const mongoose = require('mongoose');

const MeetingSchema = new mongoose.Schema({
  content: {
    type: String,
    required: true
  },
  meetingDate: {
    type: Date,
    required: true
  },
  place: {
    type: String
  },
  idClient: {
    type: ObjectId,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const Meeting = mongoose.model('meetings', MeetingSchema);

module.exports = Meeting;
