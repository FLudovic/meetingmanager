const mongoose = require('mongoose');
const { EmailValidator, NameValidator, PasswordValidator} = require('../common/validators');
const { EmailErrorMessages, NameErrorMessages, PasswordErrorMessages } = require('../common/messages');

const OrganizationSchema = require("../models/organization-model");
const { ObjectId } = require('mongodb');

const sm = require("../configs/status-messages");
const {compareDates, logger}  = require("../common/shared");
const c = require('../configs/colors');

const AdminSchema = new mongoose.Schema({
  firstName: {
    type: String,
    validate: {
        validator: NameValidator,
        message: props => NameErrorMessages(props)  
    },
    required: true
  },
  lastName: {
    type: String,
    validate: {
        validator: NameValidator,
        message: props => NameErrorMessages(props)
    },
    required: true
  },
  password: {
    type: String,
    validate: {
      validator: PasswordValidator,
      message: props => PasswordErrorMessages(props)
  },
    required: true
  },
  email: {
    type: String,
    validate: {
        validator: EmailValidator,
        message: props => EmailErrorMessages(props)  
    },
    required: true,
    index: {
      unique: true
    }
  },
  apiKey: {
    type: String,
    required: true
  },
  token: {
    type: String, 
    required: true
  },
  creationDate: {
    type: Date,
    default: Date.now
  },
  lastUpdate: {
    type: Date,
    default: "1970-01-01T01:00:00.000Z"
  },
  createdBy: {
    type: ObjectId,
    required: true
  }
});

AdminSchema.pre('deleteOne', async function(next) {
  // This refers to the request, get _id
  const id = this.getQuery()._id;
  await OrganizationSchema.deleteMany({idAdmin: id}).then((res) => {
    console.log("delete one on admin model success");
    next();
  }).catch((err) => {
    console.log("delete one on admin model error" + err);
  });
});

const Admin = mongoose.model('admins', AdminSchema);

module.exports = Admin;
