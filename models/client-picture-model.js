const { ObjectId } = require('mongodb');
const mongoose = require('mongoose');

const ClientPictureSchema = new mongoose.Schema({
  picturePath: {
    type: String
  },
  idAdmin: {
    type: ObjectId,
    required: true
  },
  idClient: {
    type: ObjectId,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const ClientPictures = mongoose.model('clientPictures', ClientPictureSchema);

module.exports = ClientPictures;
