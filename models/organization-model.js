const { ObjectId } = require('mongodb');
const mongoose = require('mongoose');

const { CompanyNameValidator } = require('../common/validators');
const { CompanyNameErrorMessages } = require('../common/messages');

const ClientSchema = require("../models/client-model");

const sm = require("../configs/status-messages");
const {compareDates, logger}  = require("../common/shared");
const c = require('../configs/colors');

const OrganizationSchema = new mongoose.Schema({
  name: {
    type: String,
    validate: {
      validator: CompanyNameValidator,
      message: props => CompanyNameErrorMessages(props)  
    },
    required: true
  },
  address: {
    type: String,
    required: true
  },
  employeeNumber: {
    type: Number,
    min: [0, 'Must be at least 0, got {VALUE}'],
    max: [100000, 'If your company has more than 100.000 employees you need to pay me. Your value : {VALUE}']
  },
  logoPath: {
    type: String,
    default: ""
  },
  idAdmin: {
    type: ObjectId,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

OrganizationSchema.pre('deleteMany', async function(next) {
  // This refers to the request, get _id
  const id = this.getQuery().idAdmin;
  // Get from admin id list of organizations which own
  await Organization.find({idAdmin: id}).then((organizationList) => {
    // Transform the organization array result in id array
    let idArray = [];
    organizationList.forEach((organization) => {
      idArray.push(organization._id);
    });
    // Delete clients with organization id
    ClientSchema.deleteMany({ idOrganization: { $in: idArray }}).then((res) => {
      console.log("delete on organization model success");
      next();
    }).catch((err) => {
      console.log("delete on organization model error" + err);
    });
  }).catch((err) => {
    console.log("delete on organization model error" + err);
  });
});

const Organization = mongoose.model('organizations', OrganizationSchema);

module.exports = Organization;
