// Import sender email
var nodemailer = require("nodemailer");
const SendmailTransport = require("nodemailer/lib/sendmail-transport");
const conf = require("../configs/conf");

// Include path
var path = require("path");

// Include fs module
const fs = require("fs");

// Log
const { logger } = require("../common/shared");
const sm = require("../configs/status-messages");

let pathToTemplate =
  path.resolve("./") + path.join("/", "email", "template.html");
let templateEmail = fs.readFileSync(pathToTemplate, {
  encoding: "utf8",
  flag: "r",
});

module.exports = {
  SendEmail: function (firstName, lastName, mailTo) {
    templateEmail = templateEmail.replace("{{firstName}}", firstName);
    templateEmail = templateEmail.replace("{{lastName}}", lastName);

    let transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: conf.user,
        pass: conf.password,
      },
    });

    let mailOptions = {
      from: "noreply.meetingmanager.team@gmail.com",
      to: mailTo,
      subject: "Welcome to Meeting manager",
      html: templateEmail,
    };

    // Send mail
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        logger(sm.sendError + error , "error");
      } else {
        logger(sm.sendSuccess + info.response , "neutral");
      }
    });
  },
};
