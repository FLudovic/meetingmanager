### Prérequis (en local)

- avoir node installé sur le poste (node --version)

### Installation (locale)

- télécharger la branche master depuis le repo git ```https://gitlab.com/FLudovic/meetingmanager.git```
- ```npm install``` pour installer les dépendances des nodes modules (librairies)
- ```npm run persist``` pour lancer le serveur avec nodemon (rechargement après sauvegarde)
- Retour console ```listening on port 4200```
- accès depuis postman sur ```https://localhost:4200```

### Installation (heroku)

- dans le terminal du projet ```heroku login``` pour se connecter sur heroku
- vérifier que l'api est up ```https://meeting-manager-666.herokuapp.com/```
- ```heroku ps:scale web=1``` pour démarrer l'api (si down)
- procéder aux tests via la collection postman fournie dans ```/tests``` du repo git

### Super Admin
Email : superAdmin@ynov.com 
Api key : b4ea1b09-859a-4c92-a361-76cf0c693894

### Auteurs

Hecquet Christian
Cerdan Guillaume
Flament Ludovic

![Blagues de dev](./_.jpg)