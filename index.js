const express    = require('express');
const mongoose   = require('mongoose');
const bodyParser = require('body-parser');
const app        = express();

const { logger } = require('./common/shared');

// Configuration files
const db = require('./configs/database').mongoURI;

// Set the port in environment file for development
// If the environment file is missing start the server
// on port 4200 by default
const PORT = process.env.PORT || 4200;

// Used to parse the response for data extraction
app.use(bodyParser.urlencoded({extended: true, limit:'100mb'}));
app.use(bodyParser.json({limit: '100mb'}));

// Set Public folder /!\ Don't forget the first '/'
app.use(express.static('apidoc'));

// Make reference to application routes
app.use('/admin', require('./routes/admin-route'));
app.use('/organization', require('./routes/organization-route'));
app.use('/connection', require('./routes/connection-route'));
app.use('/client', require('./routes/client-route'));
app.use('/meeting', require('./routes/meeting-route'));
app.use('/clientPicture', require('./routes/client-picture-route'));
app.use('/logs', require('./routes/log-route'));

// leave this line at the end of the routing because you break everything bro
app.use('/', function (req, res) {
        res.sendFile(__dirname + "/apidoc/index.html");
});

// Connect to MongoDB
mongoose.connect(db,{ useNewUrlParser: true , useUnifiedTopology: true})
        .then(() => logger ('Listening database on port 27017', 'info'))
        .catch(err => console.log(err));

app.listen(PORT, () => { logger('Listening on port ' + PORT, 'info'); });