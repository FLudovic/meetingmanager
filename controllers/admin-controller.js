// Manage environment variables
const conf = require("../configs/conf");

// Security library
const bcrypt = require("bcrypt");
// Import jsonwebtoken to manage token
const { generateToken } = require("../common/auth");

// Controls object id validity
var ObjectId = require("mongodb").ObjectId;

// Import models
const AdminSchema = require("../models/admin-model");

const { defaultNbPagination } = require("../common/shared");

// Get shared modules
const { logger, compareDates } = require("../common/shared");
const sm = require("../configs/status-messages");
const { v4: uuidv4 } = require("uuid");

// Email
const { SendEmail } = require("../email/email");

class AdminController {
  static async getAllAdministrators(token_id, callback, page) {
    // Get all administrators and filter on last inserted
    await AdminSchema.find()
      .limit(defaultNbPagination)
      .skip(page * defaultNbPagination)
      .sort({ _id: -1 })
      .then((res) => {
        if (res.length === 0) {
          // On empty result
          logger(sm.getAdmins + sm.noContent, "neutral", token_id);
          // --> Return the result to the callback
          // --> 204 don't display the return message
          callback(204, { message: "" });
        } else {
          // On success
          logger(sm.getAdmins, "neutral", token_id);
          callback(200, res);
        }
      })
      .catch((err) => {
        // On database error
        logger(err.message, "error");
        callback(500, { message: sm.getAdminsError });
      });
  }

  static async getAdministratorsByEmail(token_id, email, callback) {
    // Get administrator by email
    await AdminSchema.findOne({ email: email })
    .sort()
    .then((res) => {
        if (res === null) {
          // On empty result, unknown email
          logger(sm.getAdminsByEmail + sm.noContent, "neutral", token_id);
          callback(404, { message: sm.getAdminsError + sm.invalidEmail });
        } else {
          // Controls if Admin has rights to get created admins
          if (res.createdBy.equals(token_id)) {
            // On success
            logger(sm.getAdminsByEmail, "neutral", token_id);
            // Return custom admin object
            callback(200, {
              "_id": res._id,
              "firstName": res.firstName,
              "lastName": res.lastName,
              "email": res.email,
              "apiKey": res.apiKey
            });
          } else {
            // invalid rights
            logger(sm.getAdminsByEmail, "neutral", token_id);
            callback(401, { message: sm.getAdminsError + sm.invalidPrivilege });
          }
        }
      })
      .catch((err) => {
        // On database error
        logger(err.message, "error");
        callback(500, { message: sm.getAdminsError });
      });
  }

  static async getAdministratorsById(token_id, id, callback) {
    // If param id format is not valid
    if (!ObjectId.isValid(id)) {
      logger(sm.getAdminsById + sm.invalidId, "warning", token_id);
      callback(400, { message: sm.getAdminsError + sm.invalidId });
    } else {
      // If param id format is valid
      // Get administrator by id
      AdminSchema.findOne({ _id: id })
      .sort()
      .then((res) => {
          if (res === null) {
            // On empty result, unknown id
            logger(sm.getAdminsById + sm.noContent, "neutral", token_id);
            callback(404, { message: sm.getAdminsError + sm.invalidId });
          } else {
            if (res.createdBy.equals(token_id)) {
              // On success
              logger(sm.getAdminsById, "neutral", token_id);
              callback(200, {
                "_id": res._id,
                "firstName": res.firstName,
                "lastName": res.lastName,
                "email": res.email,
                "apiKey": res.apiKey
              });
            } else {
              // invalid rights
              logger(sm.getAdminsById, "neutral", token_id);
              callback(401, { message: sm.getAdminsError + sm.invalidPrivilege });
            }
          }
        })
        .catch((err) => {
          // On database error
          logger(err.message, "error");
          callback(500, { message: sm.getAdminsError });
        });
    }
  }

  static async saveAdministrator(token_id, body, callback) {
    try {
      // Find last administrator to controls if admin has rights to update
      const hasRights = await AdminSchema.findById(token_id).then((res) => {
        if (compareDates(res.lastUpdate, new Date(Date.now()))) {
          
          // If body is missing
          if (Object.keys(body).length === 0) {
            logger(sm.saveAdmin + sm.invalidBody, "warning", token_id);
            callback(400, {message: sm.saveAdminError + sm.invalidBody });
          } else {
            // If body exists
            const apiKey = uuidv4();

            // map user fields
            const newAdmin = new AdminSchema({
              firstName: body.firstName,
              lastName: body.lastName,
              email: body.email,
              password: body.password,
              apiKey: apiKey,
              createdBy: token_id
            });

            // Create token
            const token = generateToken(newAdmin);
            // Append the model with the new token
            newAdmin.token = token;

            // Password
            //Generate random salt for every transaction
            let saltMin = parseInt(conf.SALT_ROUND_MIN);
            let saltMax = parseInt(conf.SALT_ROUND_MAX);
            let saltRound = Math.floor(Math.random() * (saltMax - saltMin)) + saltMin;

            // Salt and Crypt password before save
            bcrypt.genSalt(saltRound, (err, salt) => {
              bcrypt.hash(
                newAdmin.password + conf.PEPPER,
                salt,
                (err, hash) => {
                  if (err) {
                    throw err;
                  } else {
                    newAdmin.password = hash;

                    // Update creation rights
                    newAdmin.lastUpdate = new Date(Date.now());

                    // Save new Admin
                    newAdmin.save().then((response) => {
                        // Update current admin lastUpdate field
                        res.lastUpdate = new Date(Date.now());

                        AdminSchema.findOneAndUpdate({ _id: res._id }, res,{ upsert: true },
                          function (err, doc) {
                            console.log("err", err);
                            console.log("doc", doc);
                          }
                        );

                        // On success
                        logger(sm.saveAdmin, "neutral", token_id);
                        callback(200, {
                          "_id": newAdmin._id,
                          "firstName": newAdmin.firstName,
                          "lastName": newAdmin.lastName,
                          "email": newAdmin.email,
                          "apiKey": newAdmin.apiKey
                        });

                        // Send mail
                        SendEmail(newAdmin.firstName,newAdmin.lastName,newAdmin.email);
                      })
                      .catch((err) => {
                        // Controls if error is type of duplicated key
                        if (err && err.code === 11000) {
                          logger(sm.saveAdmin + sm.duplicatedEmail,"warning",token_id);
                          callback(409, { message: sm.saveAdminError + sm.duplicatedEmail });
                        } else if (err && err.errors) {
                          // Else return validation error with field
                          if (err && err.message) {
                            // TODO Manage server errors here
                            logger(sm.saveAdmin + err.message,"warning",token_id);
                            callback(400, { message: err.message });
                          } else {
                            // Return message on unmanaged errors
                            logger(err.message, "error");
                            callback(500, { message: sm.saveAdminError });
                          }
                        }
                    });
                  }
                }
              );
            });
          }
        } else {
          // On invalid quotas
          logger(sm.saveAdmin + sm.invalidQuotas, "warning", token_id);
          callback(401, { message: sm.saveAdminError + sm.invalidQuotas });
        }
      });
    } catch (err) {
      // On database error
      logger(err.message, "error");
      callback(500, { message: sm.saveAdminError });
    }
  }

  static async updateAdministratorById(token_id, body, id, callback) {
    // If body is missing
    if (Object.keys(body).length === 0) {
      logger(sm.updateAdmin + sm.invalidBody, "warning", token_id);
      callback(400, { message: sm.updateAdminError + sm.invalidBody });
    } else {
      // If param id format is not valid
      if (!ObjectId.isValid(id)) {
        logger(sm.updateAdmin + sm.invalidId, "warning", token_id);
        callback(400, { message: sm.updateAdminError + sm.invalidId });
      } else {

        try {
          await AdminSchema.findById(id).then((res) => {
            if (!res.createdBy.equals(token_id)) {
              // invalid rights
              logger(sm.updateAdmin, "neutral", token_id);
              callback(401, { message: sm.updateAdminError + sm.invalidPrivilege });
            } else {
              // map user fields
              const newAdmin = {
                firstName: body.firstName,
                lastName: body.lastName,
                email: body.email,
                password: body.password,
              };
      
              const options = {
                runValidators: true,
                context: "query",
                returnOriginal: false,
              };
      
              // Update admin with the new model and activate validation options from schema
              AdminSchema.findOneAndUpdate({ _id: id }, newAdmin,options, function (err, res) {
                  if (!err) {
                    if (res === null) {
                      logger(sm.updateAdmin + sm.invalidUpdate, "warning", token_id);
                      callback(404, { message: sm.updateAdminError + sm.invalidUpdate });
                    } else {
                      // On success
                      logger(sm.updateAdmin, "neutral", token_id);
                      callback(200, {
                        "_id": res._id,
                        "firstName": res.firstName,
                        "lastName": res.lastName,
                        "email": res.email
                      });
                    }
                  } else {
                    if (err && err.code === 11000) {
                      // On duplicated email
                      logger(sm.updateAdmin + err.message, "warning", token_id);
                      callback(409, { message: sm.updateAdminError + sm.duplicatedEmail });
                    } else {
                      // On validation error
                      logger(sm.updateAdmin + err.message, "warning", token_id);
                      callback(400, { message: err.message });
                    }
                  }
                }
              );
            }
          });
        } catch (err) {
          // On database error
          logger(err.message, "error");
          callback(500, { message: sm.updateAdminsError });
        } 
      }
    }
  }

  static async deleteAdministratorById(token_id, id, callback) {
    try {
      await AdminSchema.findById(id).then((res) => {
        if (res.createdBy.equals(token_id)) {
          // If param id is not valid
          if (!ObjectId.isValid(id)) {
              logger(sm.invalidId, "warning", token_id);
              callback(400, { message: sm.deleteAdminError + sm.invalidId});
          } else {
              AdminSchema.deleteOne({ _id: id }).then((res) => {
                  // On success
                  logger(sm.deleteAdmin, "neutral", token_id);
                  callback(200, { message: sm.deleteSuccess});
              }).catch((err) => {
                  // On error
                  logger(err.message, "error");
                  callback(500, { message: sm.deleteAdminError});
              });
          }       
        } else {
          // invalid rights
          logger(sm.deleteAdmin, "neutral", token_id);
          callback(401, { message: sm.deleteAdminError + sm.invalidPrivilege });
        }
      });
    } catch (err) {
      // On error
      logger(err.message, "error");
      callback(500, { message: sm.deleteAdminError});
    }
  }
  
  
}

module.exports = { AdminController };