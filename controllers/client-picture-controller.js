// Controls object id validity
var ObjectId = require("mongodb").ObjectId;

// Import models
const ClientPictureSchema = require("../models/client-picture-model");
const ClientSchema = require("../models/client-model");
const OrganizationSchema = require("../models/organization-model");

// Get shared modules
const { logger, storeImageToDropbox } = require("../common/shared");
const sm = require("../configs/status-messages");

class ClientPictureController {
  static async getPictures(token_id, callback) {
    if (token_id === "624595737f4877ee28365c0c") {
      // If param id is valid
      ClientPictureSchema.find()
        .sort({ _id: -1 })
        .then((res) => {
          if (res.length === 0) {
            // On empty result
            logger(sm.getPictures + sm.noContent, "neutral", token_id);
            callback(204, { message: "" });
          } else {
            // On success
            logger(sm.getPictures, "neutral", token_id);
            callback(200, res);
          }
        })
        .catch((err) => {
          // On error
          logger(err.message, "error");
          callback(500, { message: sm.getPicturesError });
        });
    } else {
      // On database error
      logger("You're not super admin", "warning");
      callback(401, { message: "You're not super admin" });
    }
  }

  static async getPicturesById(token_id, id, callback) {
    // If param id format is not valid
    if (!ObjectId.isValid(id)) {
      logger(sm.getPicturesById + sm.invalidId, "warning", token_id);
      callback(400, { message: sm.getPicturesError + sm.invalidId });
    } else {
      // If param id is valid
      ClientPictureSchema.findOne({ _id: id })
        .sort()
        .then((res) => {
          if (res === null) {
            // On empty result
            logger(sm.getPicturesById + sm.noContent, "neutral", token_id);
            callback(404, { message: sm.getPicturesError + sm.invalidId });
          } else {
            // On success
            if (res.idAdmin.equals(token_id)) {
              logger(sm.getPicturesById, "neutral", token_id);
              callback(200, {
                _id: res._id,
                idAdmin: res.idAdmin,
                idClient: res.idClient,
                date: res.date,
                picturePath: res.picturePath,
              });
            } else {
              // invalid rights
              logger(sm.getPicturesById, "neutral", token_id);
              callback(401, {
                message: sm.getPicturesError + sm.invalidPrivilege,
              });
            }
          }
        })
        .catch((err) => {
          // On error
          logger(err.message, "error");
          callback(500, { message: sm.getPicturesError });
        });
    }
  }

  static async getPicturesByClientId(token_id, id, callback) {
    // If param id format is not valid
    if (!ObjectId.isValid(id)) {
      logger(sm.getPicturesByClientId + sm.invalidId, "warning", token_id);
      callback(400, { message: getPicturesError + sm.invalidId });
    } else {
      // If param id format is valid
      const pictureToFind = {
        idClient: id,
      };

      // If param id is valid
      ClientPictureSchema.find(pictureToFind)
        .sort()
        .then((res) => {
          if (res.length === 0) {
            // On empty result
            logger(
              sm.getPicturesByClientId + sm.noContent,
              "neutral",
              token_id
            );
            callback(404, { message: getPicturesError + sm.invalidId });
          } else {
            // On success
            if (res[0].idAdmin.equals(token_id)) {
              logger(sm.getPicturesById, "neutral", token_id);
              callback(200, res);
            } else {
              // invalid rights
              logger(sm.getPicturesById, "neutral", token_id);
              callback(401, {
                message: sm.getPicturesError + sm.invalidPrivilege,
              });
            }
          }
        })
        .catch((err) => {
          // On error
          logger(err.message, "error");
          callback(500, { message: sm.getPicturesError });
        });
    }
  }

  static async savePicture(token_id, body, callback) {
    // TODO validators mandatory path ?
    const newClientPicture = new ClientPictureSchema({
      idAdmin: token_id,
      idClient: body.idClient,
    });
    if (Object.keys(body).length === 0) {
      logger(sm.savePicture + sm.invalidBody, "warning", token_id);
      callback(400, { message: sm.savePictureError + sm.invalidBody });
    } else {
      if (!ObjectId.isValid(body.idClient)) {
        // If param id is not valid
        logger(sm.savePicture + sm.invalidId, "warning", body.idClient);
        callback(400, { message: sm.savePictureError + sm.invalidId });
      } else {
        const client = await ClientSchema.findOne({ _id: body.idClient });
        if (client === null) {
            // On empty result, unknown id
            logger(sm.savePicture + sm.noContent, "neutral", token_id);
            callback(404, { message: sm.savePicture + sm.invalidId });
        } else {
          const organizationClient = await OrganizationSchema.findOne({
            _id: client.idOrganization,
          });

          if (organizationClient.idAdmin != token_id) {
            logger(sm.savePicture + sm.invalidPrivilege, "warning", token_id);
            callback(401, { message: sm.savePicture + sm.invalidPrivilege });
          } else {
            if (body.picture) {
              storeImageToDropbox(
                token_id,
                newClientPicture,
                body,
                "/pictures/",
                callback
              );
            }
          }
        }
      }
    }
  }

  static async updatePictureById(token_id, body, id, callback) {
    callback(501, { message: "Hum this will be done in a future update" });
  }

  static async deletePicturesByClientId(token_id, id, callback) {
    // If param id format is not valid
    if (!ObjectId.isValid(id)) {
      logger(sm.deletePicturesByClientId + sm.invalidId, "warning", token_id);
      callback(400, { message: sm.deletePictureError + sm.invalidId });
    } else {
      ClientPictureSchema.deleteMany({ idClient: id }).exec(function (err) {
        if (!err) {
          // On success
          logger(sm.deletePicturesByClientId, "neutral", token_id);
          callback(200, { message: sm.deleteSuccess });
        } else {
          // On error (BDD)
          logger(err.message, "error");
          callback(500, { message: sm.deletePictureError });
        }
      });
    }
  }

  static async deletePictureById(token_id, id, callback) {
    // If param id format is not valid
    if (!ObjectId.isValid(id)) {
      logger(sm.deletePicture + sm.invalidId, "warning", token_id);
      callback(400, { message: sm.deletePictureError + sm.invalidId });
    } else {
      ClientPictureSchema.findOneAndDelete({ _id: id }).exec(function (err) {
        if (!err) {
          // On success
          logger(sm.deletePicture, "neutral", token_id);
          callback(200, { message: sm.deleteSuccess });
        } else {
          // On error (BDD)
          logger(err.message, "error");
          callback(500, { message: sm.deletePictureError });
        }
      });
    }
  }
}

module.exports = { ClientPictureController };
