
const fs = require("fs");
class LogController {
  static async getLogs(callback) {
    let logs = [];
    // Selects file to read
    const allFileContents = fs.readFileSync(__dirname + "/../public/logs/log.txt", "utf-8");
    // Reads file line by line
    allFileContents.split(/\r?\n/).forEach((line) => {
      if (line !== '') {
        // Do nothing if split empty line
        logs.push(JSON.parse(line));
      }
    });
    // Gets the 100 last lines
    logs = logs.slice(Math.max(logs.length - 100, 1));
    callback(200, logs);
  }
}
module.exports = { LogController };