// Import model
const MeetingSchema = require("../models/meeting-model");
const ClientSchema = require("../models/client-model");
const OrganizationSchema = require("../models/organization-model");

// Controls object id validity
const ObjectId = require("mongodb").ObjectId;

const { defaultNbPagination } = require("../common/shared");

// Log
const { logger } = require("../common/shared");
const sm = require("../configs/status-messages");
const Organization = require("../models/organization-model");

class MeetingController {
  static async getAllMeetings(token_id, callback, page) {
    if (token_id === "624595737f4877ee28365c0c") {
      // Filter on last inserted
      MeetingSchema.find()
        .limit(defaultNbPagination)
        .skip(page * defaultNbPagination)
        .sort({ _id: -1 })
        .then((res) => {
          if (res.length === 0) {
            // On empty result
            logger(sm.getMeetings + sm.noContent, "neutral", token_id);
            // --> Return the result to the callback
            // --> 204 don't display the return message
            callback(204, { message: "" });
          } else {
            // On success
            logger(sm.getMeetings, "neutral", token_id);
            callback(200, res);
          }
        })
        .catch((err) => {
          // On database error
          logger(err.message, "error");
          callback(500, { message: sm.getMeetingsError });
        });
    } else {
      // On database error
      logger("You're not super admin", "warning");
      callback(401, { message: "You're not super admin" });
    }
  }

  static async getMeetingsById(token_id, id, callback) {
    // If param id format is not valid
    if (!ObjectId.isValid(id)) {
      logger(sm.getMeetingsById + sm.invalidId, "warning", token_id);
      callback(400, { message: sm.getMeetingsError + sm.invalidId });
    } else {
      // If param id is valid
      MeetingSchema.findOne({ _id: id })
        .sort()
        .then((res) => {
          if (res === null) {
            // On empty result
            logger(sm.getMeetingsById + sm.noContent, "neutral", token_id);
            callback(404, { message: sm.getMeetingsError + sm.invalidId });
          } else {
            try {
              ClientSchema.findById(res.idClient)
                .then((client) => {
                  OrganizationSchema.findById(client.idOrganization)
                    .then((organization) => {
                      if (organization.idAdmin.equals(token_id)) {
                        // On success
                        logger(sm.getMeetingsById, "neutral", token_id);
                        callback(200, {
                          _id: res._id,
                          content: res.content,
                          meetingDate: res.meetingDate,
                          place: res.place,
                          date: res.date,
                        });
                      } else {
                        // invalid rights
                        logger(sm.getMeetingById, "neutral", token_id);
                        callback(401, {
                          message: sm.getMeetingsError + sm.invalidPrivilege,
                        });
                      }
                    })
                    .catch((err) => {
                      // On meeting error
                      logger(err.message, "error");
                      callback(500, { message: sm.getMeetingsError });
                    });
                })
                .catch((err) => {
                  // On meeting error
                  logger(err.message, "error");
                  callback(500, { message: sm.getMeetingsError });
                });
            } catch (err) {
              // On database error
              logger(err.message, "error");
              callback(500, { message: sm.getMeetingsError });
            }
          }
        })
        .catch((err) => {
          // On meeting error
          logger(err.message, "error");
          callback(500, { message: sm.getMeetingsError });
        });
    }
  }

  static async getMeetingsByClientId(token_id, id, callback) {
    // If param id format is not valid
    if (!ObjectId.isValid(id)) {
      logger(sm.getMeetingsByClientId + sm.invalidId, "warning", token_id);
      callback(400, { message: sm.getMeetingsError + sm.invalidId });
    } else {
      MeetingSchema.find({ idClient: id })
        .sort()
        .then((res) => {
          if (res.length === 0) {
            // On empty result
            logger(
              sm.getMeetingsByClientId + sm.noContent,
              "neutral",
              token_id
            );
            callback(404, { message: sm.getMeetingsError + sm.invalidId });
          } else {
            ClientSchema.findById(res[0].idClient)
              .then((client) => {
                OrganizationSchema.findById(client.idOrganization)
                  .then((organization) => {
                    if (organization.idAdmin.equals(token_id)) {
                      // On success
                      logger(sm.getMeetingsByClientId, "neutral", token_id);
                      callback(200, res);
                    } else {
                      // invalid rights
                      logger(sm.getMeetingsByClientId, "neutral", token_id);
                      callback(401, {
                        message: sm.getMeetingsError + sm.invalidPrivilege,
                      });
                    }
                  })
                  .catch((err) => {
                    // On meeting error
                    logger(err.message, "error");
                    callback(500, { message: sm.getMeetingsError });
                  });
              })
              .catch((err) => {
                // On meeting error
                logger(err.message, "error");
                callback(500, { message: sm.getMeetingsError });
              });
          }
        })
        .catch((err) => {
          // On database error
          logger(err.message, "error");
          callback(500, { message: sm.getMeetingsError });
        });
    }
  }

  static async saveMeeting(token_id, body, callback) {
    if (Object.keys(body).length === 0) {
      logger(sm.saveClient + sm.invalidBody, "warning", token_id);
      callback(400, { message: sm.saveClientError + sm.invalidBody });
    } else {
      ClientSchema.findById(body.idClient)
        .then((client) => {
          OrganizationSchema.findById(client.idOrganization)
            .then((organization) => {
              if (organization.idAdmin.equals(token_id)) {
                const newMeeting = new MeetingSchema({
                  content: body.content,
                  meetingDate: body.meetingDate,
                  place: body.place,
                  idClient: body.idClient,
                });

                newMeeting
                  .save()
                  .then((res) => {
                    // On success
                    logger(sm.saveMeeting, "neutral", token_id);
                    callback(200, {
                      _id: res._id,
                      content: res.content,
                      meetingDate: res.meetingDate,
                      place: res.place,
                      date: res.date,
                    });
                  })
                  .catch((err) => {
                    // On validation error
                    logger(err.message, "error");
                    callback(500, { message: err.message });
                  });
              } else {
                // invalid rights
                logger(sm.saveMeeting, "neutral", token_id);
                callback(401, {
                  message: sm.saveMeetingError + sm.invalidPrivilege,
                });
              }
            })
            .catch((err) => {
              // On meeting error
              logger(err.message, "error");
              callback(500, { message: sm.saveMeetingError });
            });
        })
        .catch((err) => {
          // On meeting error
          logger(err.message, "error");
          callback(500, { message: sm.saveMeetingError });
        });
    }
  }

  static async updateMeetingById(token_id, body, id, callback) {
    // If body is missing
    if (Object.keys(body).length === 0) {
      logger(sm.updateMeeting + sm.invalidBody, "warning", token_id);
      callback(400, { message: sm.updateMeetingError + sm.invalidBody });
    } else {
      ClientSchema.findById(body.idClient)
        .then((client) => {
          OrganizationSchema.findById(client.idOrganization)
            .then((organization) => {
              if (organization.idAdmin.equals(token_id)) {
                // If param id format is not valid
                if (!ObjectId.isValid(id)) {
                  logger(sm.updateMeeting + sm.invalidId, "warning", token_id);
                  callback(400, {
                    message: sm.updateMeetingError + sm.invalidId,
                  });
                } else {
                  // map user fields
                  const newMeeting = {
                    content: body.content,
                    meetingDate: body.meetingDate,
                    place: body.place,
                    idClient: body.idClient,
                  };

                  const options = {
                    runValidators: true,
                    context: "query",
                    returnOriginal: false,
                  };

                  // Update meeting with the new model and activate validation options from schema
                  MeetingSchema.findOneAndUpdate(
                    { _id: id },
                    newMeeting,
                    options,
                    function (err, meetingUpdated) {
                      if (!err) {
                        // On success
                        logger(sm.updateMeeting, "neutral", token_id);
                        callback(200, {
                          _id: meetingUpdated._id,
                          content: meetingUpdated.content,
                          meetingDate: meetingUpdated.meetingDate,
                          place: meetingUpdated.place,
                          date: meetingUpdated.date,
                        });
                      } else {
                        // On validation error
                        logger(err.message, "error");
                        callback(400, { message: err.message });
                      }
                    }
                  );
                }
              } else {
                // invalid rights
                logger(sm.saveMeeting, "neutral", token_id);
                callback(401, {
                  message: sm.updateMeetingError + sm.invalidPrivilege,
                });
              }
            })
            .catch((err) => {
              // On meeting error
              logger(err.message, "error");
              callback(500, { message: sm.updateMeetingError });
            });
        })
        .catch((err) => {
          // On meeting error
          logger(err.message, "error");
          callback(500, { message: sm.updateMeetingError });
        });
    }
  }

  static async deleteMeetingById(token_id, id, callback) {
    // If param id format is not valid
    if (!ObjectId.isValid(id)) {
      logger(sm.deleteMeeting + sm.invalidId, "warning", token_id);
      callback(400, { message: sm.deleteMeetingError + sm.invalidId });
    } else {
      MeetingSchema.findById(id)
        .then((meeting) => {
          ClientSchema.findById(meeting.idClient)
            .then((client) => {
              OrganizationSchema.findById(client.idOrganization).then(
                (organization) => {
                  // if has rights
                  if (organization && organization.idAdmin.equals(token_id)) {
                    MeetingSchema.findOneAndDelete({ _id: id }).exec(function (
                      err
                    ) {
                      if (!err) {
                        // On success
                        logger(sm.deleteMeeting, "neutral", token_id);
                        callback(200, { message: sm.deleteSuccess });
                      } else {
                        // On error (BDD)
                        logger(err.message, "error");
                        callback(500, { message: sm.deleteMeetingError });
                      }
                    });
                  } else {
                    // On invalid account
                    logger(
                      sm.deleteClient + sm.invalidPrivilege,
                      "warning",
                      token_id
                    );
                    callback(401, {
                      message: sm.deleteClientError + sm.invalidPrivilege,
                    });
                  }
                }
              );
            })
            .catch((err) => {
              // On meeting error
              logger(err.message, "error");
              callback(500, { message: sm.deleteMeetingError });
            });
        })
        .catch((err) => {
          // On meeting error
          logger(err.message, "error");
          callback(500, { message: sm.deleteMeetingError });
        });
    }
  }
}

module.exports = { MeetingController };
