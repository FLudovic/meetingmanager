// Controls object id validity
var ObjectId = require("mongodb").ObjectId;

// Import models
const OrganizationSchema = require("../models/organization-model");

const { defaultNbPagination } = require('../common/shared');

// Get shared modules
const { logger, storeLogoToDropbox, updateLogoToDropbox } = require("../common/shared");
const sm = require("../configs/status-messages");


class OrganizationController {

    static async getAllOrganizations(token_id, callback, page) {    
        if (token_id === "624595737f4877ee28365c0c") {
            // Get all organizations and filter on last inserted
            await OrganizationSchema.find().limit(defaultNbPagination).skip(page*defaultNbPagination)
            .sort({ _id: -1 }).then((res) => {
                if (res.length === 0) {
                    // On empty result
                    logger(sm.getOrganizations + sm.noContent, "neutral", token_id);
                    // --> Return the result to the callback
                    // --> 204 don't display the return message
                    callback(204, { message: '' }); 
                } else {
                    // On success
                    logger(sm.getOrganizations, "neutral", token_id);
                    callback(200, res);
                }
            }).catch((err) => {
                // On database error
                logger(err.message, "error");
                callback(500, { message: sm.getOrganizationsError });
            });
        } else {
            // On database error
            logger("You're not super admin", "warning");
            callback(401, { message: "You're not super admin" });
        }
        
    }

    static async getOrganizationsById(token_id, id, callback) {
        // If param id format is not valid
        if (!ObjectId.isValid(id)) {
            logger(sm.getOrganizationsById + sm.invalidId, "warning", token_id);
            callback(400, { message: sm.getOrganizationsError + sm.invalidId });
        } else {
            // If param id is valid
            // Get organization by id
            OrganizationSchema.findOne({ _id: id }).sort().then((res) => {
                if (res === null) {
                    // On empty result, unknown id
                    logger(sm.getOrganizationsById + sm.noContent, "neutral", token_id);
                    callback(404, { message: sm.getOrganizationsError + sm.invalidId }); 
                } else {
                    if (res.idAdmin.equals(token_id)) {
                        // On success
                        logger(sm.getOrganizationsById, "neutral", token_id);
                        callback(200, {
                            "_id": res._id,
                            "name": res.name,
                            "address": res.address,
                            "employeeNumber": res.employeeNumber,
                            "date": res.date,
                            "logoPath": res.logoPath
                        }); 
                    } else {
                        // invalid rights
                        logger(sm.getOrganizationsById, "neutral", token_id);
                        callback(401, { message: sm.getOrganizationsError + sm.invalidPrivilege });
                    }
                } 
            }).catch((err) => {
                // On error
                logger(err.message, "error");
                callback(500, { message: sm.getOrganizationsError });
            });
        }
    }

    static async getOrganizationsByAdminId(token_id, callback) {
        // If param id format is not valid
        if (!ObjectId.isValid(token_id)) {
            logger(sm.getOrganizationsByAdminId + sm.invalidId, "warning", token_id);
            callback(400, { message: sm.getOrganizationsByAdminId + sm.invalidId });
        } else {
            OrganizationSchema.find({ idAdmin: token_id }).sort().then((res) => {
                if (res.length === 0) {
                    // On empty result
                    logger(sm.getOrganizationsByAdminId + sm.noContent, "neutral", token_id);
                    callback(404, { message: sm.getOrganizationsError + sm.invalidId });
                } else {
                    // On success
                    logger(sm.getOrganizationsByAdminId, "neutral", token_id);
                    callback(200, res); 
                }
            }).catch((err) => {
                // On database error
                logger(err.message, "error");
                callback(500, { message: sm.getOrganizationsError });
            });
        }
    }

    static async saveOrganization(token_id, body, callback) {
        // If body is missing
        if (Object.keys(body).length === 0) {
            logger(sm.saveOrganization + sm.invalidBody, "warning", token_id);
            callback(400, {message: sm.saveOrganizationError + sm.invalidBody });
        } else {
            
            const newOrganization = new OrganizationSchema({
                name          : body.name,
                address       : body.address,
                employeeNumber: body.employeeNumber,
                idAdmin       : token_id
            });
        
            if (body.image) {
                storeLogoToDropbox(token_id, newOrganization, body.image, "/logos/", callback);
            } else {
                newOrganization.save().then((res) => {
                    // On success
                    logger(sm.saveOrganization, "neutral", token_id);
                    callback(200, {
                        "_id": res._id,
                        "name": res.name,
                        "address": res.address,
                        "employeeNumber": res.employeeNumber,
                        "logoPath": res.logoPath,
                        "date": res.date
                    });
                }).catch((err) => {
                    // On error
                    logger(err.message, 'error');
                    callback(400, { message: err.message });
                });
            }
        
            
        }
    }

    static async updateOrganizationById(token_id, body, id, callback) {
        // If body is missing
        if (Object.keys(body).length === 0) {
            logger(sm.updateOrganization + sm.invalidBody, "warning", token_id);
            callback(400, { message: sm.updateOrganizationError + sm.invalidBody });
        } else {
            // If param id format is not valid
            if (!ObjectId.isValid(id)) {
                logger(sm.updateOrganization + sm.invalidId, "warning", token_id);
                callback(400, { message: sm.updateOrganizationError + sm.invalidId });
            } else {

                try {
                    // Find organization to update and controls if admin has rights to update organization
                    const organizationRights = await OrganizationSchema.findOne({ "_id": id });

                    // if has rights
                    if (organizationRights && organizationRights.idAdmin.equals(token_id)) {
                            updateLogoToDropbox(token_id, body, id, "/logos/", callback);
                    } else {
                        // On invalid account
                        logger(sm.updateOrganization + sm.invalidPrivilege, "warning", token_id);
                        callback(401, { message: sm.updateOrganizationError + sm.invalidPrivilege });
                    }
                } catch (err) {
                    // error on find to check rights
                    logger(err.message, "error");
                    callback(500, { message: sm.updateOrganizationError});
                }

            }
        }        
    }

    static async deleteOrganizationById(token_id, id, callback) {      
        // If param id format is not valid
        if (!ObjectId.isValid(id)) {
            logger(sm.deleteOrganization + sm.invalidId, "warning", token_id);
            callback(400, { message: sm.deleteOrganizationError + sm.invalidId  });
        } else {

            try {
                // Find organization to update to controls if admin has rights to update
                const organizationRights = await OrganizationSchema.findOne({ "_id": id });

                // if has rights
                if (organizationRights.idAdmin.equals(token_id)) {
                    OrganizationSchema.deleteMany({ _id: id }).exec(function (err) {
                        if (!err) {
                            // On success
                            logger(sm.deleteOrganization, "neutral", token_id);
                            callback(200, { message: sm.deleteSuccess });
                        } else {
                            // On error (BDD)
                            logger(err.message, "error");
                            callback(500, { message: sm.deleteOrganizationError });
                        }
                    });
                } else {
                    // On invalid account
                    logger(sm.deleteOrganization + sm.invalidPrivilege, "warning", token_id);
                    callback(401, { message: sm.deleteOrganizationError + sm.invalidPrivilege });
                }
            } catch (err) {
                // error on async find to check rights
                logger(err.message, "error");
                callback(500, { message: sm.deleteOrganizationError });
            }
        }         
    }
}

module.exports = { OrganizationController };