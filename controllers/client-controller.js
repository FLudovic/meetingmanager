// Manage environment variables
const conf = require("../configs/conf");

// Security library
const bcrypt = require("bcrypt");
// Import jsonwebtoken to manage token
const { generateToken } = require("../common/auth");

const { defaultNbPagination } = require("../common/shared");

// Controls object id validity
var ObjectId = require("mongodb").ObjectId;

// Import models
const ClientSchema = require("../models/client-model");
const OrganizationSchema = require("../models/organization-model");

// Get shared modules
const { logger, compareDates } = require("../common/shared");
const sm = require("../configs/status-messages");

// Email
const { SendEmail } = require("../email/email");

class ClientController {
  static async getClients(token_id, callback, page) {
    if (token_id === "624595737f4877ee28365c0c") {
      await ClientSchema.find()
        .limit(defaultNbPagination)
        .skip(page * defaultNbPagination)
        .sort({ _id: -1 })
        .then((res) => {
          if (res.length === 0) {
            // On empty result
            logger(sm.getClients + sm.noContent, "neutral", token_id);
            callback(204, { message: "" });
          } else {
            // On success
            logger(sm.getClients, "neutral", token_id);
            callback(200, res);
          }
        })
        .catch((err) => {
          logger(err.message, "error");
          callback(500, { message: sm.getClientsError });
        });
    } else {
      // On database error
      logger("You're not super admin", "warning");
      callback(401, { message: "You're not super admin" });
    }
  }

  static async getClientByEmail(token_id, email, callback) {
    ClientSchema.findOne({ email: email })
      .sort()
      .then((res) => {
        if (res === null) {
          // On empty result
          logger(sm.getClientsByEmail + sm.noContent, "neutral", token_id);
          callback(404, { message: sm.getClientsError + sm.invalidEmail });
        } else {
          try {
            OrganizationSchema.findById(res.idOrganization).then(
              (organization) => {
                if (organization.idAdmin.equals(token_id)) {
                  // On success
                  logger(sm.getClientsByEmail, "neutral", token_id);
                  callback(200, {
                    _id: res._id,
                    firstName: res.firstName,
                    lastName: res.lastName,
                    email: res.email,
                    phone: res.phone,
                    date: res.date,
                  });
                } else {
                  // invalid rights
                  logger(sm.getClientsByEmail, "neutral", token_id);
                  callback(401, {
                    message: sm.getClientsError + sm.invalidPrivilege,
                  });
                }
              }
            );
          } catch (err) {
            // On error
            logger(err.message, "error");
            callback(500, { message: sm.getClientsError });
          }
        }
      })
      .catch((err) => {
        // On error
        logger(err.message, "error");
        callback(500, { message: sm.getClientsError });
      });
  }

  static async getClientById(token_id, clientId, callback) {
    if (!ObjectId.isValid(clientId)) {
      // If param id is not valid
      logger(sm.getClientsById + sm.invalidId, "warning", token_id);
      callback(400, { message: sm.getClientsError + sm.invalidId });
    } else {
      ClientSchema.findOne({ _id: clientId })
        .sort()
        .then((res) => {
          if (res === null) {
            // On empty result
            logger(sm.getClientsById + sm.noContent, "neutral", token_id);
            callback(404, { message: sm.getClientsError + sm.invalidId });
          } else {
            try {
              OrganizationSchema.findById(res.idOrganization).then(
                (organization) => {
                  if (organization.idAdmin.equals(token_id)) {
                    // On success
                    logger(sm.getClientsById, "neutral", token_id);
                    callback(200, {
                      _id: res._id,
                      firstName: res.firstName,
                      lastName: res.lastName,
                      email: res.email,
                      phone: res.phone,
                      date: res.date,
                    });
                  } else {
                    // invalid rights
                    logger(sm.getClientById, "neutral", token_id);
                    callback(401, {
                      message: sm.getClientsError + sm.invalidPrivilege,
                    });
                  }
                }
              );
            } catch (err) {
              // invalid rights
              logger(sm.getClientById, "neutral", token_id);
              callback(401, {
                message: sm.getClientsError + sm.invalidPrivilege,
              });
            }
          }
        })
        .catch((err) => {
          logger(err.message, "error");
          callback(500, { message: sm.getClientsError });
        });
    }
  }

  static async saveClient(token_id, body, callback) {
    // TODO validators error if idOrganization is not given but it's a body param the error must
    // TODO be send by the database check if it's possible to change error return mode.
    // If body is missing
    if (Object.keys(body).length === 0) {
      logger(sm.saveClient + sm.invalidBody, "warning", token_id);
      callback(400, { message: sm.saveClientError + sm.invalidBody });
    } else {
      // Wrong id organization
      if (!ObjectId.isValid(body.idOrganization)) {
        console.log("body.idOrganization", body.idOrganization);
        logger(sm.saveClient + sm.invalidIdOrganization, "warning", token_id);
        callback(400, {
          message: sm.saveClientError + sm.invalidIdOrganization,
        });
      } else {
        // Find if organization exist in bdd and if this admin has the good right
        const OrganizationById = await OrganizationSchema.findOne({
          _id: body.idOrganization,
          idAdmin: token_id,
        });

        if (OrganizationById === undefined || OrganizationById === null) {
          logger(sm.saveClient + sm.invalidIdOrganization, "warning", token_id);
          callback(400, {
            message: sm.saveClientError + sm.invalidIdOrganization,
          });
        } else {
          try {
            // Find organization to save and controls if admin has rights to save organization
            const organizationRights = await OrganizationSchema.findOne({
              _id: body.idOrganization,
            });

            // if has rights
            if (
              organizationRights &&
              organizationRights.idAdmin.equals(token_id)
            ) {
              // map user fields
              const newClient = new ClientSchema({
                firstName: body.firstName,
                lastName: body.lastName,
                age: body.age,
                phone: body.phone,
                email: body.email,
                idOrganization: body.idOrganization,
              });

              // Save new Client
              newClient
                .save()
                .then((res) => {
                  // On success
                  logger(sm.saveClient, "neutral", token_id);
                  callback(200, {
                    _id: res._id,
                    firstName: res.firstName,
                    lastName: res.lastName,
                    email: res.email,
                    phone: res.phone,
                    date: res.date,
                  });
                })

                .catch((err) => {
                  // Controls if error is type of duplicated key
                  if (err && err.code === 11000) {
                    logger(
                      sm.saveClient + sm.duplicatedEmail,
                      "warning",
                      token_id
                    );
                    callback(409, {
                      message: sm.saveClientError + sm.duplicatedEmail,
                    });
                  } else if (err && err.errors) {
                    // Else return validation error with field
                    if (err && err.message) {
                      logger(sm.saveClient + err.message, "warning", token_id);
                      callback(400, { message: err.message });
                    } else {
                      // Return message on unmanaged errors
                      logger(err.message, "error");
                      callback(500, { message: sm.getClientsError });
                    }
                  }
                });
            } else {
              // On invalid account
              logger(sm.saveClient + sm.invalidPrivilege, "warning", token_id);
              callback(401, {
                message: sm.saveClientError + sm.invalidPrivilege,
              });
            }
          } catch (err) {
            // error on find to check rights
            logger(err.message, "error");
            callback(500, { message: sm.saveClientError });
          }
        }
      }
    }
  }

  static async updateClientById(token_id, body, clientId, callback) {
    // If body is missing
    if (Object.keys(body).length === 0) {
      logger(sm.updateClient + sm.invalidBody, "warning", token_id);
      callback(400, { message: sm.updateClientError + sm.invalidBody });
    } else {
      // If param id is not valid
      if (!ObjectId.isValid(clientId)) {
        logger(sm.updateClient + sm.invalidId, "warning", token_id);
        callback(400, { message: sm.updateClientError + sm.invalidId });
      } else {
        try {
          // get client
          const client = await ClientSchema.findById(clientId);
          if (client === null) {
            logger(sm.updateClient + sm.invalidId, "warning", token_id);
            callback(400, { message: sm.updateClientError + sm.invalidId });
          } else {
            // Find organization to save and controls if admin has rights to save organization
            const organizationRights = await OrganizationSchema.findOne({
              _id: client.idOrganization,
            });

            // if has rights
            if (
              organizationRights &&
              organizationRights.idAdmin.equals(token_id)
            ) {
              // map user fields
              const newClient = {
                firstName: body.firstName,
                lastName: body.lastName,
                age: body.age,
                phone: body.phone,
                email: body.email,
              };

              const options = {
                runValidators: true,
                context: "query",
                returnOriginal: false,
              };

              // Update Client with the new model and activate validation options from schema
              ClientSchema.findOneAndUpdate(
                { _id: clientId },
                newClient,
                options,
                function (err, res) {
                  if (!err) {
                    if (res === null) {
                      logger(
                        sm.updateClient + sm.invalidUpdate,
                        "warning",
                        token_id
                      );
                      callback(404, {
                        message: sm.updateClientError + sm.invalidUpdate,
                      });
                    } else {
                      // On success
                      logger(sm.updateClient, "neutral", token_id);
                      callback(200, {
                        _id: res._id,
                        firstName: res.firstName,
                        lastName: res.lastName,
                        email: res.email,
                        phone: res.phone,
                        date: res.date,
                      });
                    }
                  } else {
                    if (err && err.code === 11000) {
                      // On duplicated email
                      logger(
                        sm.updateClient + err.message,
                        "warning",
                        token_id
                      );
                      callback(409, {
                        message: sm.updateClientError + sm.duplicatedEmail,
                      });
                    } else {
                      // On validation error
                      logger(err.message, "warning");
                      callback(400, { message: err.message });
                    }
                  }
                }
              );
            } else {
              // On invalid account
              logger(
                sm.updateClient + sm.invalidPrivilege,
                "warning",
                token_id
              );
              callback(401, {
                message: sm.updateClientError + sm.invalidPrivilege,
              });
            }
          }
        } catch (err) {
          // error on find to check rights
          logger(err.message, "error");
          callback(500, { message: sm.updateClientError });
        }
      }
    }
  }

  static async deleteClientById(token_id, clientId, callback) {
    // If param id is not valid
    if (!ObjectId.isValid(clientId)) {
      logger(sm.deleteClient + sm.invalidId, "warning", token_id);
      callback(400, { message: sm.deleteClientError + sm.invalidId });
    } else {
      try {
        // Find organization to save and controls if admin has rights to save organization
        await ClientSchema.findById(clientId).then((client) => {
          if (client == null) {
            // On empty result, unknown id
            logger(sm.deleteClient + sm.noContent, "neutral", token_id);
            callback(404, { message: sm.deleteClientError + sm.invalidId });
          } else {
            OrganizationSchema.findOne({ _id: client.idOrganization }).then(
              (organization) => {
                // if has rights
                if (organization && organization.idAdmin.equals(token_id)) {
                  ClientSchema.deleteMany({ _id: clientId }).exec(function (
                    err
                  ) {
                    if (!err) {
                      // On success
                      logger(sm.deleteClient, "neutral", token_id);
                      callback(200, { message: sm.deleteSuccess });
                    } else {
                      // On error
                      logger(err.message, "error");
                      callback(500, { message: sm.deleteClientError });
                    }
                  });
                } else {
                  // On invalid account
                  logger(
                    sm.deleteClient + sm.invalidPrivilege,
                    "warning",
                    token_id
                  );
                  callback(401, {
                    message: sm.deleteClientError + sm.invalidPrivilege,
                  });
                }
              }
            );
          }
        });
      } catch (err) {
        // error on find to check rights
        logger(err.message, "error");
        callback(500, { message: sm.deleteClientError });
      }
    }
  }
}

module.exports = { ClientController };
