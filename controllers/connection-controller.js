// Manage environment variables
const conf = require("../configs/conf");

// Security library
const bcrypt = require("bcrypt");

// Import jsonwebtoken to manage token
const { generateToken } = require("../common/auth");

// Controls object id validity
var ObjectId = require("mongodb").ObjectId;

// Import models
const AdminSchema = require("../models/admin-model");

// Get shared modules
const { logger } = require("../common/shared");
const sm         = require("../configs/status-messages");

class ConnectionController {

    static async connectionLogin(body, callback) {
        try {
            // Get admin
            const { email, apiKey } = body;
        
            // Validate admin input
            if (!(email && apiKey)) {
              logger(sm.login, 'neutral', email);
              callback(404, { message: sm.loginError + sm.invalidInputs }); 

            } else {
              // Check user exist in database
              const admin = await AdminSchema.findOne({ email });
        
              if (admin && (apiKey == admin.apiKey)) {
                // Create token
                const tokenValue = generateToken(admin);
        
                // If param id format is not valid 
                if (!ObjectId.isValid(admin.id)) {
                    logger(sm.login + sm.invalidId, "warning", email);
                    callback(400, { message: sm.loginError + sm.invalidId }); 

                } else {
                    const options = { runValidators: true, context: "query", returnOriginal: false};

                    // Update admin with the new model and activate validation options from schema
                    AdminSchema.findOneAndUpdate({ _id: admin.id }, {'$set': {token : tokenValue}}, options, function (err, adminUpdated) {
                        if (!err) {
                            // On success
                            logger(sm.login, "neutral", email);
                            callback(200, { "token": adminUpdated.token });        
                        } else {
                            // On error (BDD error)
                            logger(err.message, "error");
                            callback(500, { message: sm.loginError });        
                        }
                    });
                }
              } else {
                logger(sm.login + sm.invalidCredentials, "warning", email);
                callback(401, { message: sm.loginError + sm.invalidCredentials });        
              }
            }
          } catch (err) {
            // Async error
            logger(err.message, "error");
            callback(500, { message: sm.loginError });                   
          }
    }

}

module.exports = { ConnectionController };